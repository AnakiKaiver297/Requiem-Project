
[![gplv3](https://img.shields.io/badge/Licensed%20under-GPLv3-blue.svg?style=flat-square)](./COPYING.md) 
[![made-with-python](https://img.shields.io/badge/Made%20with-Python%203.7-ffde57.svg?longCache=true&style=flat-square&colorB=ffdf68&logo=python&logoColor=88889e)](https://www.python.org/) 
[![made-with-discord.py](https://img.shields.io/badge/Using-discord.py-ffde57.svg?longCache=true&style=flat-square&colorB=4584b6&logo=discord&logoColor=7289DA)](https://github.com/Rapptz/discord.py)
[![requiem-support](https://img.shields.io/discord/457720586160963604.svg?style=flat-square&label=Requiem%20Support)](https://discord.gg/MHDVxfH)
[![Project Stats](https://top.gg/api/widget/servers/406643604019347456.png)](https://top.gg/bot/406643604019347456)

# Requiem Project

*Requiem* is a free to use and open source discord bot designed to provide a large number of tools one convenient, easy
to use package. Requiem prides itself on being the largest politics and war discord bot on the market! With a large
(and growing) number of features, Requiem is sure to have something that will make running your alliance (and nation)
easier!


## Getting Started

The public version of Requiem can be invited to your server using this [invite](https://discordapp.com/oauth2/authorize?client_id=406643604019347456&scope=bot&permissions=18496)!
For the best experience, its advised you don't remove the default permissions as set by the invite link! Don't
worry though! Requiem only requires these three permissions to function!
* Send Messages
* Embed Links
* Add Reactions

You can find a full list of available modules by running `r!help`

![help1](https://i.imgur.com/7tBWfos.png)

You can get a full list of commands for a module by running `r!help <module_name>` 
For example: `r!help politics and war`

![help2](https://imgur.com/2KZidEl.png)

You can get the help text for a specific command using `r!help <command_name>`
For example: `r!help alliance`

![help3](https://imgur.com/QZtvMFw.png)

Note that argument types are denoted using `[]` and `<>` where `[]` indicates a required argument and `<>` indicates an
optional argument. Do not include them in command execution! For example, the alliance command would be run like so:
`r!alliance North Point`

![help4](https://imgur.com/cYawR48.png)


If you prefer to self-host Requiem for security reasons, you can find information about that in [**Self Hosting**](https://gitlab.com/AnakiKaiver297/Requiem-Project/wikis/Self-Hosting).
