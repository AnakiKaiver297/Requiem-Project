CREATE TABLE IF NOT EXISTS exceptions(
    reference       INTEGER         PRIMARY KEY,
    traceback       TEXT            NOT NULL,
    context         TEXT            NOT NULL,
    occurrences     INTEGER         NOT NULL
)