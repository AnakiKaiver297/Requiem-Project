CREATE TABLE IF NOT EXISTS guilds(
    guild_id            BIGINT              PRIMARY KEY,
    prefix              TEXT                NOT NULL,
    embed_colour        TEXT                NOT NULL,
    greet_channel       BIGINT              DEFAULT 0,
    greet_message       TEXT                DEFAULT NULL,
    greet_dm            BOOLEAN             DEFAULT False,
    greet_dm_message    TEXT                DEFAULT NULL,
    auto_role           BIGINT              DEFAULT 0,
    leave_channel       BIGINT              DEFAULT 0,
    leave_message       TEXT                DEFAULT NULL,
    offensive_monitor   JSON                DEFAULT '{}',
    defensive_monitor   JSON                DEFAULT '{}',
    applicant_monitor   JSON                DEFAULT '{}',
    beige_monitor       JSON                DEFAULT '{}',
    tags                JSON                DEFAULT '{}'
)