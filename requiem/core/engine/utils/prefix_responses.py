
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


responses = [
    lambda ctx: f"Looking for me? My prefix on this guild is ``{ctx.loc_prefix}``",
    lambda ctx: f"Need something? My prefix on this guild is ``{ctx.loc_prefix}``",
    lambda ctx: f"If you forget my prefix, you can always ping me! My prefix on this guild is ``{ctx.loc_prefix}``",
    lambda ctx: f"Did you know you can ping me to execute commands. For example, '<@!{ctx.bot.user.id}> ping' to "
                f"execute the 'ping' command! My prefix on this guild is ``{ctx.loc_prefix}``",
    lambda ctx: f"Whats up **{ctx.author.name}**? My prefix on this guild is ``{ctx.loc_prefix}``",
    lambda ctx: f"My prefix on this guild is ``{ctx.loc_prefix}``"
]
