
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from .funcs import build_reactions, timeout_handler
import asyncio


class Menunator:

	def __init__(self, ctx, message, pages: list, reacts: list, timeout=300):
		self.ctx = ctx
		self.bot = ctx.bot
		self.message = message
		self.reacts = reacts
		self.built = self.build(reacts, pages)
		self.timeout = timeout
		self.failed = False
		self.chosen = None
		self.current_time = 0

	def check(self, reaction, user):
		if user.id != self.ctx.author.id:
			return False
		if user.bot:
			return False
		if reaction.emoji not in self.built.keys():
			return False
		if not reaction.message.id == self.message.id:
			return False
		return True

	async def navigate(self):

		self.bot.loop.create_task(build_reactions(self))
		hdl_reacts_add = self.bot.loop.create_task(self.handle_reaction_add())
		await timeout_handler(self)
		hdl_reacts_add.cancel()

		if self.failed:
			raise self.failed
		return self.chosen

	async def handle_reaction_add(self):
		while True:
			try:
				reaction, user = await self.bot.wait_for("reaction_add", timeout=self.timeout, check=self.check)
				await self.handle_reaction(reaction, user)
			except asyncio.TimeoutError:
				pass

	async def handle_reaction(self, reaction, user):
		if reaction.emoji in self.built.keys():
			self.chosen = self.built[reaction.emoji]
			self.current_time = self.timeout

	@staticmethod
	def build(reacts, pages):
		final = {}
		if len(pages) == len(reacts):
			page_number = 0
			for page in pages:
				final[reacts[page_number]] = page
				page_number += 1
		return final
