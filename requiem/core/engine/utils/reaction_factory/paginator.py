
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from .funcs import build_reactions, timeout_handler
import asyncio
import discord


class Paginator:

	def __init__(self, bot, target, author, pages: list, timeout=300):
		self.bot = bot
		self.target = target
		self.author = author
		self.pages = pages
		self.timeout = timeout
		self.current_time = 0
		self.reacts = ['⏮', '◀', '🔒', '❎', '▶', '⏭']
		self.current_page = 0
		self.next_page = 0
		self.message = None
		self.locked = True
		self.failed = False

	def check(self, reaction, user):
		if not reaction.message.id == self.message.id:
			return False

		if self.locked and user.id != self.author.id:
			return False

		if user.bot:
			return False

		if reaction.emoji not in self.reacts:
			return False

		return True

	async def navigate(self):

		self.message = await self.target.send(embed=self.pages[0])

		if len(self.pages) > 1:

			self.bot.loop.create_task(build_reactions(self))
			hdl_reacts_add = self.bot.loop.create_task(self.handle_reaction_add())
			hdl_reacts_remove = self.bot.loop.create_task(self.handle_reaction_remove())
			await timeout_handler(self)
			hdl_reacts_add.cancel()
			hdl_reacts_remove.cancel()

		if self.failed:
			raise self.failed

	async def handle_reaction_add(self):
		while True:
			try:
				reaction, user = await self.bot.wait_for("reaction_add", timeout=self.timeout, check=self.check)
				await self.handle_reaction(reaction, user)
				await self.change_pages()

			except asyncio.TimeoutError:
				pass

	async def handle_reaction_remove(self):
		while True:
			try:
				reaction, user = await self.bot.wait_for("reaction_remove", timeout=self.timeout, check=self.check)
				await self.handle_reaction(reaction, user)
				await self.change_pages()

			except asyncio.TimeoutError:
				pass

	async def change_pages(self):
		if self.next_page != self.current_page:
			self.current_time = 0
			await self.message.edit(embed=self.pages[self.next_page])
			self.current_page = self.next_page

	async def handle_reaction(self, reaction, user):

		if reaction.emoji == '◀':
			if self.current_page == 0:
				self.next_page = len(self.pages) - 1
			else:
				self.next_page -= 1

		elif reaction.emoji == '⏮':
			if self.current_page == 0:
				self.next_page = len(self.pages) - 1
			else:
				self.next_page = 0

		elif reaction.emoji == '▶':
			if self.current_page == len(self.pages) - 1:
				self.next_page = 0
			else:
				self.next_page += 1

		elif reaction.emoji == '⏭':
			if self.current_page == len(self.pages) - 1:
				self.next_page = 0
			else:
				self.next_page = len(self.pages) - 1

		elif reaction.emoji == '❎' and user.id == self.author.id:
			try:
				await self.message.delete()
			except discord.errors.NotFound:
				pass
			self.current_time = self.timeout

		elif reaction.emoji in ['🔒', '🔓'] and user.id == self.author.id:
			if self.locked:
				self.locked = False
				self.reacts = ['⏮', '◀', '🔓', '❎', '▶', '⏭']
			else:
				self.locked = True
				self.reacts = ['⏮', '◀', '🔒', '❎', '▶', '⏭']
			try:
				await self.message.clear_reactions()
			except discord.errors.Forbidden:
				pass
			except discord.errors.NotFound:
				self.current_time = self.timeout
