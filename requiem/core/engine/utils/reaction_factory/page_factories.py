
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from .funcs import line_check, length_check
import discord


def build_embed_pages(text, lines=20, length=30, align=False, title=None, color=discord.Colour.purple()):
	if lines > 20:
		lines = 20
	if length > 50:
		length = 50

	output = length_check(text, length)
	pages = line_check(output, lines)
	embedded_pages = []

	for page in pages:
		if align:
			page = f"```{page}```"

		embed = discord.Embed(
			title=title,
			description=page,
			color=color
		)

		embedded_pages.append(embed)

	return embedded_pages


def build_string_pages(text, lines=20, length=30):
	if lines > 25:
		lines = 25

	if length > 40:
		length = 40

	output = length_check(text, length)
	pages = line_check(output, lines)
	return pages
