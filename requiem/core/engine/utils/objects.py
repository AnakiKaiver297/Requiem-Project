
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from discord.ext import commands
from . import extra_funcs


class Config:
    """Takes a json or other iterable object and makes it into an object with callable attributes"""

    def __init__(self, entry):
        for i in entry.keys():
            self.__setattr__(i, entry[i])


class Context(commands.Context):
    """Custom context object for Requiem"""

    @property
    def guild_config(self):
        """Retrieves the current guilds config"""
        return self.bot.get_guild_config(self.guild.id)

    @property
    def user_config(self):
        """Retrieves the authors config"""
        return self.bot.get_user_config(self.author.id)

    @property
    def loc_prefix(self):
        """Retrieves the local prefix"""
        if self.guild:
            prefix = self.guild_config.prefix
        else:
            prefix = self.bot.config.prefix
        return prefix

    @property
    def colour(self):
        """Gets the colour for the location this interaction takes place"""
        if self.guild:
            colour = self.guild_config.embed_colour
        else:
            colour = "purple"
        colour = extra_funcs.get_colour(colour)
        return colour

    def get_user_config(self, user_id):
        """Gets a config belonging to a user besides the author"""
        return self.bot.get_user_config(user_id)


class Cog(commands.Cog):
    tasks: list = []


class Credentials:
    """Credentials Object for Requiem"""

    def __init__(self, data):
        self.token = data["token"]
        self.owners = data["owners"]
        self.postgres_database = data["postgres_database"]
        self.postgres_password = data["postgres_password"]
        self.postgres_user = data["postgres_user"]
        self.postgres_port = data["postgres_port"]
        self.postgres_host = data["postgres_host"]
        self.prefix = data["prefix"]
        self.joinex = data["joinex"]
        self.logging_level = data["logging_level"]
        self.fail_count = data["fail_count"]
        self.silence_exceptions = data["silence_exceptions"]
        self.debug = data["debug"]
        self.auto_update = data["auto_update"]
        self.version_url = data["version_url"]
        self.custom_prefixes = data["custom_prefixes"]
        self.case_insensitive = data["case_insensitive"]
        self.dbl_token = data["dbl_token"]
        self.pnw_email = data["pnw_email"]
        self.pnw_pass = data["pnw_password"]
        self.pnw_token = data["pnw_token"]
        self.statuses = data["statuses"]
        self.introduction = data["introduction"]
