
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from os import walk
from json import load, dump
from sys import platform


def search(target):
    """Searches all files and folders in the bots directory for a matching file"""
    for (directory, directories, files) in walk("."):
        for file in files:
            if file == target:
                return directory, target
    return None, None


def fetch(target):
    """Fetches the contents of a given file"""
    location, target = search(target)

    if platform == "win32":
        syn = "\\"
    else:
        syn = "/"

    if location:
        location = location.replace(f".{syn}", "")
        with open(f"{location}{syn}{target}") as result:
            if ".json" in target:
                return load(result)
            else:
                return result.read()
    return None


def save(location, target, deliver):
    """Sets/Creates a file with a given location and file name"""
    location = location.replace(".", "")
    with open(location + target, 'w') as result:
        if isinstance(deliver, (dict, list)):
            dump(deliver, result)
        else:
            result.write(str(deliver))
        result.close()
