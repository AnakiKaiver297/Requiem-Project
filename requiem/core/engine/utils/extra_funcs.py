
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import traceback
import asyncio
from pwpy import scrape
from random import randint
from discord import Colour
from discord.ext import commands


async def get_prefix(bot, message):
    """Retrieves the bots prefix"""
    if f"<@!{bot.user.id}> " in message.content and len(message.content.split()) > 1:
        return f"<@!{bot.user.id}> "
    elif f"<@{bot.user.id}> " in message.content and len(message.content.split()) > 1:
        return f"<@{bot.user.id}> "

    if message.guild:
        async with bot.pool.acquire() as conn:
            prefix = await conn.fetchval(
                f"SELECT prefix FROM guilds WHERE guild_id = ($1)", message.guild.id
            )

    else:
        prefix = bot.config.prefix

    return prefix


async def find_nation_by_user(ctx, target):
    try:
        if target:
            user = await commands.UserConverter().convert(ctx, target)
        else:
            user = ctx.author
        config = ctx.bot.get_user_config(user.id)
        target = config.nation_id
    except (commands.BadArgument, AttributeError):
        user = None

    nation = await scrape.lookup(ctx.bot.pw.nations_latest, id=target, name=target)

    if nation:
        if user:
            leader = user.mention
        else:
            try:
                conf = ctx.get_user_config
                leader = [u for u in ctx.guild.members if conf(
                    u.id) and conf(u.id).nation_id == nation.id][0].mention
            except (IndexError, AttributeError):
                leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
                         f"receiver={nation.leader.replace(' ', '%20')})"
    else:
        leader = None

    return nation, user, leader


def get_traceback(error):
    """Gets a full traceback from a given error"""
    error = error.__cause__ or error
    tb = traceback.format_exception(
        type(error), error, error.__traceback__, chain=False)
    tb = "".join(tb)

    return tb


def get_colour(colour):
    """Retrieves a colour from the dict below and returns the object it refers to"""
    colours = {
        "teal": Colour.teal(),
        "dark teal": Colour.dark_teal(),
        "green": Colour.green(),
        "dark green": Colour.dark_green(),
        "blue": Colour.blue(),
        "dark blue": Colour.dark_blue(),
        "purple": Colour.purple(),
        "dark purple": Colour.dark_purple(),
        "magenta": Colour.magenta(),
        "dark magenta": Colour.dark_magenta(),
        "gold": Colour.gold(),
        "dark gold": Colour.dark_gold(),
        "orange": Colour.orange(),
        "dark orange": Colour.dark_orange(),
        "red": Colour.red(),
        "dark red": Colour.dark_red(),
        "dark grey": Colour.dark_grey(),
        "light grey": Colour.light_grey(),
        "random": randint(0, 0xFFFFFF)
    }
    if colour in colours.keys():
        colour = colours[colour.lower()]
    elif colour == "list":
        colour = colours
    else:
        colour = None
    return colour


def stringify(lst, end):
    """Converts a list into a string with proper punctuation and formatting"""
    if len(lst) > 2:
        fmt = f'{", ".join(lst[:-1])}, {end} {lst[-1]}'
    else:
        fmt = f' {end} '.join(lst)

    fmt = fmt.replace("_", " ")

    return fmt


async def execute(command):
    """Executes a given command asynchronously"""
    proc = await asyncio.create_subprocess_shell(
        command,
        stdout=asyncio.subprocess.PIPE,
        stderr=asyncio.subprocess.PIPE
    )
    stdout, stderr = await proc.communicate()
    return stdout.decode("utf-8"), stderr.decode("utf-8")
