
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
from .client import BotClient
from .utils import resource_handler, extra_funcs, objects
import asyncio
import json
from sys import platform


class Runner:
    """
    Runner that gets basic config and set up logger before starting the bot
    """

    def __init__(self):

        # configures the logger temporarily until credentials are retrieved.
        log_format = f'%(asctime)s - %(name)s - %(levelname)s: %(message)s'
        date_format = "%H:%M"
        logging.basicConfig(
            level=logging.INFO,
            format=log_format,
            datefmt=date_format
        )
        logger = logging.getLogger("runner")

        # a final catch for all exceptions. if this exception point is reached, it should be reported to the developers.
        try:

            # attempts to get the bots credentials. if none are found, it creates the file.
            logger.info("Getting credentials!")
            config = resource_handler.fetch("credentials.json")
            creds_template = resource_handler.fetch("credentials_template")

            while True:
                if not config:
                    output = "Unable to locate credentials! A new file has been created... " \
                             "Please take a moment to fill out the new file!"
                    resource_handler.save(
                        ".", "credentials.json", creds_template)

                elif config == json.loads(creds_template):
                    output = "You appear to have not filled out the credentials.json file! " \
                             "Please take a moment to fill out the file before continuing!"

                else:
                    try:
                        config = objects.Credentials(config)
                        logger.info(
                            "Credentials retrieved... Proceeding with startup!")
                        del creds_template
                        break
                    except KeyError as error:
                        cause = error.args[0]
                        output = f"The key {cause} was not found in the credentials.json file! Please validate you "\
                                 f"haven't changed or removed any keys and try again!\nIf you can't find the problem, "\
                                 f"simply delete the file and Requiem will create a new one!"

                logger.error(output)
                input("Press enter to continue!")
                config = resource_handler.fetch("credentials.json")

            # attempts to set the bots event loop policy.
            if platform == "linux" or platform == "linux2":
                try:
                    from uvloop import EventLoopPolicy

                    asyncio.set_event_loop_policy(EventLoopPolicy())
                    logger.info("Using uvloop for asyncio event loop policy.")
                    del EventLoopPolicy

                except ModuleNotFoundError:
                    logger.critical(
                        "Requiem was unable to gather uvloop for the asyncio event loop policy! "
                        "Please ensure that it is installed by running the command 'python3 -m pip install uvloop' in the terminal!")
                    input("Press enter to continue!")

            elif platform == "win32":
                asyncio.set_event_loop(asyncio.ProactorEventLoop())
                logger.info("Using ProactorEventLoop for asyncio event loop.")

            else:
                logger.critical(
                    "Requiem was unable to determine the host operating system! This is likely because the operating system is not yet supported!"
                    "Requiem is unable to start!")
                input(".....")
                exit()

            # attempts to init and run the bot client
            bot = BotClient(config)

            # configures the logger for the remainder of this instance
            logging.basicConfig(
                level=config.logging_level,
                format=log_format,
                datefmt=date_format
            )

            bot.run()

        except RuntimeError:
            pass
        except Exception as error:
            tb = extra_funcs.get_traceback(error)
            logger.critical(f"A critical exception was raised! If this continues, "
                            f"please report it to the dev team! {tb}")
            input("Press enter to continue!")
