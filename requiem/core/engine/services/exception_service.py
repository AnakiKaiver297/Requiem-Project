
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
from core.engine.utils import extra_funcs, err_resps
import sys
import asyncio
import discord
from discord.ext.commands import errors
import random
import traceback


class ExceptionService:
    """
    Exception handler for all non discord related exceptions
    """

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger("requiem.exception_service")
        self.bot.on_error = self.catch_event_exception
        self.bot.add_listener(self.catch_command_exception, "on_command_error")
        self.bot.loop.create_task(self.exception_aware_monitor())
        self.fail_safe = {"tasks": {}, "functions": {}}
        self.fail_count = self.bot.config.fail_count

    async def catch_command_exception(self, ctx, error):
        """Catches command errors that aren't CommandInvokeErrors"""

        if isinstance(error, errors.CommandInvokeError) and error.original:
            error = error.original

        general_message = random.choice(err_resps.satire)
        error_name = error.__class__.__name__

        if hasattr(error, "__module__"):
            module = error.__module__
        else:
            module = "builtins"

        if module in err_resps.exceptions.keys() and error_name in err_resps.exceptions[module].keys():
            caught = True
            error_message = err_resps.exceptions[module][error_name]
            if error_message:
                if callable(error_message):
                    error_message = error_message(error, ctx)
                response = error_message
            else:
                response = None
        else:
            caught = False
            response = f"{general_message}\n\nAn unexpected error occurred! Sorry about that!"

        if not response:
            return

        elif caught:
            await self.alert_user(ctx, response)

        else:

            if ctx.command.name in self.fail_safe["functions"].keys():
                self.fail_safe["functions"][ctx.command.name] += 1
            else:
                self.fail_safe["functions"][ctx.command.name] = 1

            tb = extra_funcs.get_traceback(error)
            context = f"Command: {ctx.command.name}\nFull Message: {ctx.message.content}\n"\
                      f"Author: {ctx.author.name}\nGuild: {str(ctx.guild)}\nChannel: {str(ctx.channel)}\n"\
                      f"Failed: {self.fail_safe['functions'][ctx.command.name]}"

            if self.fail_safe["functions"][ctx.command.name] == self.fail_count:
                self.bot.remove_command(ctx.command.name)
                context += "\nThis command has exceeded the fail safe and has been disabled!"

            log, reference = await self.build_log_details(tb, context)
            config = self.bot.config
            owners = config.owners

            if config.debug and ctx.author.id in owners:
                response = f"Debug Mode - Exception Code {reference}\nException details have been dumped to logs! " \
                            "Alternatively, you can use the 'exception' command to get the details here!"

            await self.alert_user(ctx, response)
            await self.report_exception(log)

    async def catch_event_exception(self, event_method, *args, **kwargs):
        """Catches all event exceptions"""
        exc_type, exc_value, exec_traceback = sys.exc_info()

        tb = traceback.format_exception(exc_type, exc_value, exec_traceback, chain=False)
        tb = "".join(tb)

        context = f"Event: {event_method}"
        log, reference = await self.build_log_details(tb, context)

        await self.report_exception(log)

    async def exception_aware_monitor(self):
        """Background process task exception handler"""
        loop = self.bot.loop

        while True:
            running = self.bot.running

            try:

                for task_group in running:

                    try:
                        tasks = running[task_group]
                        done, pending = await asyncio.wait(
                            tasks.keys(), loop=loop, return_when=asyncio.FIRST_EXCEPTION, timeout=1
                        )

                        for task in done:
                            if task.exception():

                                try:
                                    self.fail_safe["tasks"][task._coro.__name__] += 1
                                except KeyError:
                                    self.fail_safe["tasks"][task._coro.__name__] = 1

                                coro, args, kwargs = tasks.pop(task)

                                tb = extra_funcs.get_traceback(task.exception())

                                fail_count = self.fail_safe["tasks"][task._coro.__name__]

                                if self.fail_safe["tasks"][task._coro.__name__] != self.fail_count:
                                    running[task_group][loop.create_task(coro())] = (coro, args, kwargs)
                                    status = "The process was restarted!"
                                else:
                                    status = "The process has hit the fail safe max! It has not been restarted..."

                                context = f"Background Process: {task._coro.__name__}\n" \
                                    f"Fail Count: {fail_count}\nStatus: {status}"
                                log, reference = await self.build_log_details(tb, context)

                                await self.report_exception(log)

                    except ValueError:
                        running.pop(task_group)

            except RuntimeError:
                await asyncio.sleep(15)

            await asyncio.sleep(1)

    @staticmethod
    async def alert_user(ctx, response):
        """Alerts the user who executed the command that a problem occurred"""
        embed = discord.Embed(
            description=response,
            color=discord.Colour.red()
        )

        try:
            await ctx.send(embed=embed, delete_after=10)
        except discord.Forbidden:
            pass

    async def report_exception(self, log_details):
        """Reports the exception to the owners"""
        config = self.bot.config
        self.logger.error(log_details)

        if not config.silence_exceptions:
            owners = config.owners

            embed = discord.Embed(description=log_details, color=discord.Colour.red())

            for owner_id in owners:
                user = self.bot.get_user(owner_id)
                try:
                    await user.send(embed=embed)
                except Exception as error:
                    self.logger.debug(f"There was an issue messaging {user.name} (SNOWFLAKE {user.id}) with the "
                                      f"error report! The error was as follows: {error}")

    async def build_log_details(self, tb, context_details):
        """Builds the log details of the exception and returns them for logging"""
        async with self.bot.database.pool.acquire() as conn:
            query = "SELECT reference, occurrences FROM exceptions WHERE traceback = ($1)"
            check = await conn.fetchrow(query, tb)

            if check:
                reference, occurrences = check
                occurrences += 1
                query = "UPDATE exceptions SET occurrences = ($1) WHERE reference = ($2)"
                await conn.execute(query, occurrences, reference)

            else:
                reference = random.randint(0, 0xFFFFFF)
                occurrences = 1
                query = "INSERT INTO exceptions(reference, traceback, context, occurrences) " \
                        "VALUES(($1), ($2), ($3), ($4))"
                await conn.execute(query, reference, tb, context_details, occurrences)

        report_details = f"An Unexpected Exception was Raised!\nLog Details---\nException Code: {reference}\n" \
            f"Occurrences: {occurrences}\nContext---\n{context_details}\nTraceback---\n{tb}"
        return report_details, reference
