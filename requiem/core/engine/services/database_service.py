
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
import asyncio
import asyncpg
import os
import discord
from core.engine.utils import objects, resource_handler, extra_funcs


class DatabaseService:
    """Connection and request manager for Postgres database"""

    def __init__(self, bot):
        self.bot = bot
        self.logger = logging.getLogger("requiem.database_service")
        self.pool = bot.loop.run_until_complete(self.start())
        self.bot.loop.run_until_complete(self.assess())
        self.bot.loop.create_task(self.build_user_configs())
        self.bot.loop.create_task(self.build_guild_configs())
        self.bot.add_listener(self.table_correct_on_ready, "on_ready")
        self.bot.add_listener(self.create_config_on_join, "on_guild_join")
        self.guild_configs = {}
        self.user_configs = {}

    async def start(self):
        """Starts requiems connection to the database pool if credentials for the postgres server are provided!"""

        config = self.bot.config
        database = config.postgres_database
        user = config.postgres_user
        password = config.postgres_password
        port = config.postgres_port
        host = config.postgres_host

        if all(len(value) > 0 for value in (database, user, password, str(port))):
            pool = await asyncpg.create_pool(
                database=database,
                user=user,
                password=password,
                port=port,
                host=host
            )
            self.logger.info(
                f"A new connection pool to {database} has been established!")
            return pool

        else:
            output = "Requiem could not establish a connection pool to the postgres server! Requiem requires a "\
                     "postgres server to build its tables in! Please ensure postgresql is installed and running and "\
                     "you have filled out the postgres credentials in the credentials.json file!"
            self.logger.critical(output)
            input("Press enter to continue!")
            exit(0)

    async def assess(self):
        """Assesses Requiems database and ensures all tables are up-to-date!"""
        self.logger.info("Requiem is running a table assessment!")
        config = self.bot.config

        latest = [file for file in os.listdir(
            "core/data/resources") if file.endswith(".sql")]
        current = resource_handler.fetch("genesis.json")

        if not current:
            current = {}
            resource_handler.save("core/data/cache/", "genesis.json", current)
            self.logger.debug(
                "The genesis.json file could not be found! The file was created in 'core/data/cache/'!")

        for table in latest:
            self.logger.debug(
                f"Running table assessment on {config.postgres_database}.{table[:-4]}!")

            check = resource_handler.fetch(table)
            if table in current.keys() and current[table] != check:
                await self.update_table(table)
                current[table] = check

            elif table not in current.keys():
                await self.create_table(table)
                current[table] = check

            else:
                await self.validate_table(table)

            self.logger.debug(
                f"The genesis.json file has been updated with table information for {table[:-4]}!")
            resource_handler.save("core/data/cache/", "genesis.json", current)

        self.logger.info("Requiem has completed its table assessment!")

    async def update_table(self, table):
        """Updates a specified table with the new sql file contents"""
        self.logger.info(f"Requiem is updating the table {table[:-4]}!")
        self.logger.warning("WARNING! CLOSING THE SCRIPT WHILE A TABLE UPDATE IS TAKING PLACE CAN RESULT IN"
                            " PERMANENT DATA LOSS!")

        alter_data = resource_handler.fetch(f"alter_{table}")

        async with self.pool.acquire() as conn:
            self.logger.debug("Connection acquired from the pool!")

            try:
                if alter_data:
                    self.logger.info(
                        f"An alter operation is being run on {table[:-4]}!")
                    await conn.execute(alter_data)

                current = await conn.fetch(f"SELECT * FROM {table[:-4]}")
                entries = []
                count = 0
                for row in current:
                    count += 1
                    self.logger.info(
                        f"Transferring entries from {table[:-4]} to memory: {count} of {len(current)}!")
                    entries.append([", ".join("$" + str(i + 1)
                                              for i in range(len(row))), row])

                await conn.execute(f"DROP TABLE {table[:-4]}")
                await self.create_table(table)

                count = 0
                for entry in entries:
                    count += 1
                    self.logger.info(f"Transferring entries from memory to {table[:-4]}: "
                                     f"Entry {count} of {len(entries)}")
                    await conn.execute(f"INSERT INTO {table[:-4]} VALUES ({entry[0]})", *entry[1])

            except asyncpg.UndefinedTableError:
                self.logger.warning(f"The table transfer of {table[:-4]} failed because the table could not be found! "
                                    f"The service is recreating it!")
                await self.create_table(table)

    async def create_table(self, table):
        """Creates a new specified table with the sql file contents"""
        self.logger.info(f"Requiem is creating the table {table[:-4]}!")

        data = resource_handler.fetch(table)
        async with self.pool.acquire() as conn:
            await conn.execute(data)

        self.logger.info(
            f"Table creation for {table[:-4]} completed successfully!")

    async def validate_table(self, table):
        """Validates that a specified table exists in the database"""
        self.logger.debug(f"Requiem is validating the table {table[:-4]}!")

        async with self.pool.acquire() as conn:
            try:
                await conn.fetch(f"SELECT * FROM {table[:-4]}")
            except asyncpg.UndefinedTableError:
                await self.create_table(table)

        self.logger.debug(f"Table {table[:-4]} validated")

    async def create_config_on_join(self, guild):
        """Creates a guild config table on guild join"""

        def channel_check(channel):
            member = discord.utils.get(guild.members, id=self.bot.user.id)
            p = channel.permissions_for(member)
            if isinstance(channel, discord.TextChannel) and all(i for i in (p.send_messages, p.embed_links)):
                return True
            return False

        channels = [
            channel for channel in guild.channels if channel_check(channel)]
        intro_channel = channels[0] if len(channels) > 0 else None
        intro = resource_handler.fetch("introduction")

        async with self.pool.acquire() as conn:
            try:
                prefix = self.bot.config.prefix
                await conn.execute("INSERT INTO guilds VALUES(($1), ($2), ($3))", guild.id, prefix, "purple")
                self.logger.info(f"A new database entry was created for the guild {guild.name} "
                                 f"(SNOWFLAKE: {guild.id})!")

                intro = intro.format(self.bot.config.prefix,
                                     self.bot.config.prefix)
                colour = extra_funcs.get_colour("purple")

            except asyncpg.UniqueViolationError:
                self.logger.info(
                    f"The database entry for guild {guild.name} (SNOWFLAKE: {guild.id}) already exists!")
                config = self.get_guild_config(guild.id)
                colour = extra_funcs.get_colour(config.embed_colour)

                intro = intro.format(config.prefix, config.prefix)
                intro = intro + "\nRequiem has been on this server before! Its configuration has been restored! If " \
                                f'you wish to reset it, use the "``{config.prefix}configreset``" command and the ' \
                                "database entry for this table will be cleared!"

        if intro_channel and self.bot.config.introduction:
            try:
                embed = discord.Embed(description=intro, colour=colour)
                await intro_channel.send(embed=embed)
            except discord.errors.Forbidden:
                pass

    async def table_correct_on_ready(self):
        """Ensures there are no missing entries in the guilds table"""
        self.logger.info("Requiem is validating database entries!")
        prefix = self.bot.config.prefix

        async with self.pool.acquire() as conn:
            data = await conn.fetch("SELECT guild_id FROM guilds")
            output = "No faults found in database entries!"
            stored = [row[0] for row in data]
            missing = 0

            for guild in self.bot.guilds:
                if guild.id not in stored:

                    missing += 1
                    self.logger.debug(
                        f"Missing entry detected in guilds! SNOWFLAKE {guild.id} has been added!")
                    output = f"{missing} database entries were missing! Entries corrected..."
                    await conn.execute("INSERT INTO guilds VALUES(($1), ($2), ($3))", guild.id, prefix, "purple")

        self.logger.info(output)

    async def build_guild_configs(self):
        """Builds a cache of the guilds table in memory for quick access"""
        self.logger.debug("Beginning cache operation for guilds table!")

        run = 0
        while True:
            run += 1
            self.logger.debug(f"Performing cache run {run} for guilds table!")

            async with self.pool.acquire() as conn:
                guild_configs = await conn.fetch(f"SELECT * FROM guilds")

            cached = {}
            for entry in guild_configs:
                count = 0
                config = {}

                for key in entry.keys():
                    config[key] = entry[count]
                    count = count + 1

                cached[entry[0]] = objects.Config(config)

            self.guild_configs = cached
            await asyncio.sleep(.01)

    async def build_user_configs(self):
        """Builds a cache of the users table in memory for quick access"""
        self.logger.debug("Beginning cache operation for guilds table!")

        run = 0
        while True:
            run += 1
            self.logger.debug(f"Performing cache run {run} for users table!")

            async with self.pool.acquire() as conn:
                guild_configs = await conn.fetch(f"SELECT * FROM users")

            cached = {}
            for entry in guild_configs:
                count = 0
                config = {}
                for key in entry.keys():
                    config[key] = entry[count]
                    count = count + 1
                cached[entry[0]] = objects.Config(config)

            self.user_configs = cached
            await asyncio.sleep(.01)

    def get_guild_config(self, target):
        """Gets a config from the database belonging to a specified guild snowflake"""
        self.logger.debug(
            f"A request for a guild config belonging to SNOWFLAKE {target} was made!")

        if target in self.guild_configs.keys():
            config = self.guild_configs[target]
            self.logger.debug("Guild config found and returned!")
            return config

        else:
            self.logger.debug("No guild config found! Returning NONE!")
            return None

    def get_user_config(self, target):
        """Gets a config from the database belonging to a specified guild snowflake"""
        self.logger.debug(
            f"A request for a user config belonging to SNOWFLAKE {target} was made!")

        if target in self.user_configs.keys():
            config = self.user_configs[target]
            self.logger.debug("User config found and returned!")
            return config

        else:
            self.logger.debug("No user config found! Returning NONE!")
            return None
