# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
import asyncio
import time


class CommandService:
    """Command execution handler"""

    def __init__(self, bot):
        self.bot = bot
        self.served = {}
        self.executed = 0
        self.logger = logging.getLogger("requiem.command_service")
        self.bot.before_invoke(self.pre_execution_run)
        self.bot.after_invoke(self.after_execution_run)

    async def pre_execution_run(self, ctx):
        config = ctx.user_config
        if not config:
            async with ctx.typing():
                async with self.bot.pool.acquire() as conn:
                    await conn.execute("INSERT INTO users VALUES(($1))", ctx.author.id)
                await asyncio.sleep(5)

        if ctx.author.id in self.served:
            self.served[ctx.author.id] += 1
        else:
            self.served[ctx.author.id] = 1
        self.executed += 1

        setattr(ctx, "run_time", time.monotonic())

    async def after_execution_run(self, ctx):
        if ctx.invoked_subcommand:
            return

        output = "Command Executed\n" \
                 f"Command: {ctx.command.name}\n" \
                 f"Command Parents: {ctx.command.parents}\n" \
                 f"Author: {ctx.author.name}\n" \
                 f"Full Message: {ctx.message.content}\n" \
                 f"Execution Time: {int(time.monotonic() - ctx.run_time)}"
        self.logger.info(output)
