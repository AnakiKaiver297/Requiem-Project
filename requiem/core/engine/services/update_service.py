
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
import platform
import asyncio
from core.engine.utils import extra_funcs


class UpdateService:
    """
    A service updater for Requiem. Pulls the latest build from the requiem repo when it detects a new one is available
    """

    def __init__(self, bot):
        self.bot = bot
        self.config = bot.config
        self.logger = logging.getLogger("requiem.update_service")
        self.bot.loop.run_until_complete(self.check_for_updates())
        self.bot.loop.create_task(self.auto_updater())

    async def check_for_updates(self):
        """checks gitlab for updates. Installs them if updates are found"""
        self.logger.info("Requiem is checking for updates!")

        await extra_funcs.execute("git fetch origin")
        stdout, stderr = await extra_funcs.execute("git status -uno")

        if "Your branch is up-to-date" in stdout:
            self.logger.info("Requiem is up-to-date!")

        elif "Your branch is ahead of" in stdout:
            self.logger.info("Current Requiem build exceeds latest REPO build!")

        elif self.config.auto_update:
            self.logger.info("Update Found!")
            await self.update_build()
            await self.update_dependencies()
            await self.finalize()

        elif any(out in stderr for out in ("git is not recognized", "not a git repository")):
            self.logger.error("Requiem is unable to check for updates! A manual update will be required!")

        else:
            self.logger.info("There are updates available for Requiem!")

    async def update_build(self):
        """Attempts to pull the latest build from the repo. Returns whether it was successful or not"""
        self.logger.info("Pulling from REPO and updating!")

        await extra_funcs.execute("git pull")

        self.logger.info("Requiem has finished pulling from the REPO!")

    async def update_dependencies(self):
        """Updates the dependencies"""
        self.logger.info("Updating dependencies!")

        if platform == "linux" or platform == "linux2":
            await extra_funcs.execute("python3 -m pip install -r requirements.txt -U")
        else:
            await extra_funcs.execute("py -3 -m pip install -r requirements.txt -U")

    async def finalize(self):
        """Grants scripting permissions to itself if on linux"""
        self.logger.info("Finalizing update! Requiem will restart...")

        if platform == "linux" or platform == "linux2":
            await extra_funcs.execute("chmod +x launcher.sh")

        await self.bot.restart()

    async def auto_updater(self):
        """Automatically checks for updates periodically"""
        self.logger.info("Auto Update Service Started!")

        while True:
            await asyncio.sleep(10000)
            await self.check_for_updates()
