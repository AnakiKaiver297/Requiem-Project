
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import logging
import aiohttp
import asyncio
import contextlib
import os
import discord
from discord.ext import commands
from datetime import timedelta
from discord.errors import LoginFailure
import time
from .utils import extra_funcs, objects, prefix_responses
import random
from . import services
from sys import platform
from __init__ import __version__


class BotClient(commands.AutoShardedBot):
    """
    Requiems backend services. A cleaner replacement for the old poorly coded Cardinal backend.
    This rewritten version is based off of BAKER which in turn was based off the old Cardinal backend.
    BAKER can be found here: https://gitlab.com/Tmpod/baker
    """

    def __init__(self, config):
        self.loop = asyncio.get_event_loop()
        self.config = config

        # determines whether to use get_prefix function or the default prefix string
        if self.config.custom_prefixes:
            command_prefix = extra_funcs.get_prefix
        else:
            command_prefix = self.config.prefix

        # inits the bot class
        super().__init__(command_prefix=command_prefix,
                         case_insensitive=self.config.case_insensitive)

        self.logger = logging.getLogger("requiem.client")

        self.logger.info(
            "Copyright (C) 2019  Requiem Project - GNU General Public License v3")
        self.logger.info(f"Version {__version__}")
        self.logger.info("Requiem is starting...")

        self.loaded_extensions = []
        self.running = {}
        self.csess = aiohttp.ClientSession()
        self.started_at = time.perf_counter()

        self.updater = services.update_service.UpdateService(self)
        self.database = services.database_service.DatabaseService(self)
        self.exceptions = services.exception_service.ExceptionService(self)
        self.execution = services.command_service.CommandService(self)

        self.pool = self.database.pool
        self.get_guild_config = self.database.get_guild_config
        self.get_user_config = self.database.get_user_config

        self.on_message = self.handle_message_events
        self.add_listener(self.handle_message_events, "on_message_edit")
        self.add_listener(self.task_on_start, "on_ready")
        self.add_check(self.bot_check)
        self.remove_command("help")

    async def task_on_start(self):
        if platform == "win32":
            syn = "\\"
        else:
            syn = "/"
        files = os.listdir(f"core{syn}data{syn}cache{syn}ytdl_downloads")
        for file in files:
            with contextlib.suppress(PermissionError):
                os.remove(
                    f"core{syn}data{syn}cache{syn}ytdl_downloads{syn}{file}")
        self.load_all_extensions()
        self.remove_listener(self.task_on_start)

    async def get_context(self, message, *, cls=objects.Context):
        """A function to overwrite the discord.py get_context function and return
        the custom Requiem context object"""
        return await super().get_context(message, cls=cls)

    def add_cog(self, cog):
        """Overwrite of the default add_cog to include starting tasks"""
        super().add_cog(cog)
        self.start_tasks(cog)

    def remove_cog(self, cog):
        """Overwrite of the default remove_cog to include stopping tasks"""
        self.stop_tasks(cog)
        super().remove_cog(cog)

    def start_tasks(self, cog):
        """Starts tasks for a given extension"""
        if len(cog.tasks) > 0:
            self.logger.debug(f"Start tasks call for cog {cog.__cog_name__}!")
            running = {self.loop.create_task(task()): (
                task, (), {}) for task in cog.tasks}
            self.running[cog.__cog_name__] = running

    def stop_tasks(self, cog):
        """Stops tasks for a given extension"""
        cog = self.get_cog(cog)
        if cog.__cog_name__ in self.running.keys():
            self.logger.debug(f"Stop tasks call for cog {cog.__cog_name__}!")
            [task.cancel() for task in self.running[cog.__cog_name__]]
            self.running.pop(cog.__cog_name__, None)

    def load_extension(self, name):
        """Loads a specified extension"""
        if name in self.all_extensions:
            try:
                super().load_extension(f"src.extensions.{name}")
                self.loaded_extensions.append(name)
                output = f"The extension {name} was loaded successfully!"
            except commands.errors.ExtensionAlreadyLoaded:
                output = f"The extension {name} is already loaded!"
            except commands.errors.ExtensionNotFound:
                output = f"The extension {name} could not be loaded because there is an import error!"
            except Exception as error:
                output = f"The extension {name} failed to load with the following error: {error}"
        else:
            output = f"The extension {name} could not be found! Please ensure it is in the directory src/extensions/" \
                f"and try again!"
        self.logger.info(output)
        return output

    def unload_extension(self, name):
        """Unloads a specified extension"""
        try:
            super().unload_extension(f"src.extensions.{name}")
            output = f"The extension {name} unloaded successfully!"
        except commands.errors.ExtensionNotLoaded:
            output = f"The extension {name} is not loaded!"
        self.logger.info(output)
        return output

    def load_all_extensions(self):
        """Loads all extensions"""
        output = ""
        for extension in self.all_extensions:
            result = self.load_extension(extension)
            output += result + "\n"
        return output

    def unload_all_extensions(self):
        """Unloads all extensions"""
        output = ""
        for extension in self.loaded_extensions:
            result = self.unload_extension(extension)
            output += result + "\n"
        return output

    @property
    def all_extensions(self):
        return [i[:-3] for i in os.listdir("src/extensions/") if i.endswith(".py") if not i == "__init__.py"]

    def run(self):
        """Runs the bot"""

        # validates the token provided and starts the bot. If the token is invalid, it restarts the bot.
        if len(self.config.token) == 0:
            self.logger.critical("There was no token provided in the credentials.json file! "
                                 "Please supply a token and try again!")
            input("Press enter to continue!")
        else:
            try:
                super().run(self.config.token)
            except LoginFailure:
                self.logger.critical("The token provided in credentials.json is invalid! "
                                     "Please supply a proper token and try again!")
                input("Press enter to continue!")

    async def stop(self):
        """Stops the bot"""
        await self.csess.close()
        self.unload_all_extensions()
        await self.logout()

    async def restart(self):
        """Exits the bot with a return code of 1. This tells the wrapper to restart the service."""
        await self.stop()
        exit(1)

    async def terminate(self):
        """Exits the bot with a return code of 0. This tells the wrapper to terminate the service."""
        await self.stop()
        exit(1011)

    @property
    def uptime(self) -> timedelta:
        """Returns the bot's uptime"""
        return timedelta(seconds=time.perf_counter() - self.started_at)

    async def bot_check(self, ctx):
        if ctx.guild:
            member = ctx.guild.get_member(self.user.id)
            p = ctx.channel.permissions_for(member)

            if not all(i for i in (p.send_messages, p.embed_links, p.add_reactions, p.read_message_history)):
                message = "In order to ensure nominal error-free operation, Requiem requires the following " \
                          "permissions: Send Messages, Embed Links, Add Reactions, and Read Message History"
                embed = discord.Embed(
                    title="Basic Permissions Missing",
                    description=message,
                    colour=ctx.colour
                )
                try:
                    await ctx.author.send(embed=embed)
                except discord.errors.Forbidden:
                    pass
                return False
        return True

    async def handle_message_events(self, message, message_edit=None):
        if message.author.bot:
            return

        if not self.is_ready():
            return

        if not message_edit:
            message_edit = message

        if len(message_edit.embeds) > 0:
            return

        content = message_edit.content
        ctx = await self.get_context(message_edit)

        if ctx.guild:
            mention = ctx.guild.get_member(self.user.id).mention
        else:
            mention = self.user.mention

        if content == mention:
            ctx = await self.get_context(message_edit)
            choice = random.choice(prefix_responses.responses)
            response = choice(ctx)
            embed = discord.Embed(description=response, color=ctx.colour)
            await ctx.send(embed=embed)

        elif any(content.lower() == f"{i} are you there?" for i in (f"<@{self.user.id}>", f"<@!{self.user.id}>")):
            embed = discord.Embed(description="I am here", colour=ctx.colour)
            await ctx.send(embed=embed)

        else:
            await self.process_commands(message_edit)
