# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_tradeprices(ctx):
    embed = discord.Embed(
        title="Trade Prices Unavailable!",
        description="Due to the unreliability of the Politics and War trade api and the amount of"\
                    " calls necessary to get current trade prices, this command has been disabled."\
                    " At current, there are no plans to reimplement this functionality unless changes"\
                    " are made to the api to make getting the needed information easier! "\
                    " https://politicsandwar.com/nation/trade/display=world",
        colour=ctx.colour                    
    )
    await ctx.send(embed=embed)
    return
    try:
        prices = await ctx.bot.pwscrape.tradeprices()
        embed = discord.Embed(
            title="Trade Prices",
            description="Current Market Averages",
            colour=ctx.colour
        )
        embed.add_field(name="Market Index",
                        value=f'${prices.index:,}', inline=False)
        embed.add_field(name="Food", value=f'${prices.food:,}', inline=False)
        embed.add_field(name="Aluminum",
                        value=f'${prices.aluminum:,}', inline=False)
        embed.add_field(name="Munitions",
                        value=f'${prices.munitions:,}', inline=False)
        embed.add_field(name="Gasoline",
                        value=f'${prices.gasoline:,}', inline=False)
        embed.add_field(name="Coal", value=f'${prices.coal:,}', inline=False)
        embed.add_field(name="Oil", value=f'${prices.oil:,}', inline=False)
        embed.add_field(
            name="Uranium", value=f'${prices.uranium:,}', inline=False)
        embed.add_field(name="Iron", value=f'${prices.iron:,}', inline=False)
        embed.add_field(
            name="Bauxite", value=f'${prices.bauxite:,}', inline=False)
        embed.add_field(name="Lead", value=f'${prices.lead:,}', inline=False)
        embed.add_field(
            name="Credits", value=f'${prices.credits:,}', inline=False)
    except Exception:
        embed = discord.Embed(
            title="Trade Prices",
            description="Requiem was unable to connect to the site for trade price retrieval! "
                        "Please try again later...",
            colour=ctx.colour
        )
    await ctx.send(embed=embed)
