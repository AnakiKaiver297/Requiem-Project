# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
import discord


async def run_bulkcitycost(ctx, citystart, cityend):
    embed = discord.Embed(
        title=f"Bulk City Cost Calculator",
        description=f"Cities {citystart} through {cityend}",
        colour=ctx.colour)
    if citystart < cityend < citystart + 21:
        try:
            cost = 0
            while citystart <= cityend:
                price = calculate.city(citystart)
                cost += price
                embed.add_field(
                    name=f"City {citystart}", value=f"${price:,.2f}")
                citystart += 1
            embed.add_field(name="Total Cost", value=f"${cost:,.2f}")
        except ValueError as error:
            embed.add_field(
                name="There was a problem with the value given!", value=f"{error}")
    else:
        message = "You can only calculate up to 20 cities at a time and your given end value must " \
                  "be greater than your start value!"
        embed.add_field(
            name="There was a problem with the value given!", value=message)
    await ctx.send(embed=embed)
