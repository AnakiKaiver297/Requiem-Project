
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import json
import discord
import re
from core.engine.utils import reaction_factory


async def run_tag(ctx):
	config = ctx.guild_config
	existing = json.loads(config.tags)
	pages = []
	fields = 0

	if len(existing) > 0:
		embed = discord.Embed(title="Available Tags", colour=ctx.colour)
		for key in existing:
			tag = key.replace("_", " ")
			if fields < 15:
				embed.add_field(name=tag, value=existing[key], inline=False)
			else:
				pages.append(embed)
				fields = 0
				embed = discord.Embed(title="Available Tags", colour=ctx.colour)
				embed.add_field(name=tag, value=existing[key], inline=False)
		if embed not in pages:
			pages.append(embed)
	else:
		embed = discord.Embed(description="There are no tags on this server!", colour=ctx.colour)
		pages.append(embed)

	await reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages).navigate()


async def run_tag_create(ctx, instr):
	config = ctx.guild_config
	existing = json.loads(config.tags)
	try:
		tag = re.search('"(.*)"', instr).group(1)
		response = instr.split(tag)[1][1:]
		if response[0] == " ":
			response = response[1:]
			if 0 < len(tag) <= 50:
				if tag not in existing.keys():
					if 0 < len(response) <= 1500:
						output = f"Alright! You've successfully created the tag **{tag}**!"
						existing[tag.replace(" ", "_")] = response.replace("'", "$@").replace('"', "$@")
						async with ctx.bot.pool.acquire() as conn:
							await conn.execute(f"UPDATE guilds SET tags = ($1) WHERE guild_id = ($2)",
											   f"{existing}".replace("'", '"'), ctx.guild.id)
					else:
						output = "Please limit tag text size to 1500 characters or less"
				else:
					output = "That tag already exists! Please pick a different tag name or delete" \
							 " the existing tag!"
			else:
				output = "Please limit the tag name size to 50 characters or less! It is preferrable that you " \
						 "keep the length of the tag short. The limit is 50 due to emoji characters taking up " \
						 "more space!"
		else:
			output = "You did not give a response! Please ensure there is a " \
					 "space between the trigger phrase and the response!"
	except AttributeError:
		output = "You did not give a phrase! Your phrase must be encased in quotes!"

	embed = discord.Embed(description=output, colour=ctx.colour)
	await ctx.send(embed=embed)


async def run_tag_delete(ctx, tag):
	config = ctx.guild_config
	existing = json.loads(config.tags)
	key = tag.replace(" ", "_")
	if key in existing.keys():
		existing.pop(key)
		async with ctx.bot.pool.acquire() as conn:
			await conn.execute(f"UPDATE guilds SET tags = ($1) WHERE guild_id = ($2)",
		                       f"{existing}".replace("'", '"'), ctx.guild.id)
		output = f"Alright! The tag **{tag}** has been deleted!"
	else:
		output = "No such tag exists!"

	embed = discord.Embed(description=output, colour=ctx.colour)
	await ctx.send(embed=embed)
