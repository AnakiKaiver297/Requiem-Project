# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import scrape, exceptions
from core.engine.utils import reaction_factory
from src.functions import utils
import random
import string
import discord
import asyncio
import aiohttp


title = "Requiem Nation Registration"


async def send_message(bot, target, user):
    token = ''.join(random.SystemRandom().choice(
        string.ascii_uppercase + string.digits) for _ in range(random.choice([6, 8, 10])))

    message = "This is an automated message sent by the Requiem Nation Registration System!\n" \
              "You are receiving this message because someone has attempted to register as you\n" \
              "with the Requiem Nation Registry!\n\n" \
              f"This Request was made by {user.name} (snowflake: {user.id})\n\n" \
              f"Your verification code is {token}\n\n" \
              "If this was not you, feel free to discard this message!"

    await bot.pwscrape.send_message(target, message, subject="Requiem Nation Registration Service")
    return token


async def cancel(ctx):
    embed = discord.Embed(
        title=title,
        description="Alright! You've canceled the registration!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    await asyncio.sleep(10)
    await utils.clean_up(message)
    return None, None


async def try_again(ctx, message):
    output = f"{message} Would you like to try again?"
    embed = discord.Embed(title=title, description=output, colour=ctx.colour)
    message = await ctx.send(embed=embed)
    check = await reaction_factory.Confirminator(ctx, message).navigate()
    await utils.clean_up(message)
    if check:
        return func_0_0, ctx
    else:
        return cancel, ctx


async def run_registerpw(ctx):
    try:
        if ctx.bot.pw.nations_latest:
            function = main, ctx
            await utils.runnable(function)
        else:
            embed = discord.Embed(
                title="PWPY is not ready yet!",
                description="PWPY has not finished caching nations! Please wait a few minutes and try again!",
                colour=ctx.colour
            )
            await ctx.send(embed=embed)
    except exceptions.NotLoggedIn:
        embed = discord.Embed(
            title="Not Logged In",
            description="Requiem lacks necessary login credentials for PW!",
            color=ctx.colour
        )
        await ctx.send(embed=embed)


async def main(ctx):
    config = ctx.user_config
    nation_id = config.nation_id

    nation = await scrape.lookup(ctx.bot.pw.nations_latest, id=nation_id)
    if nation:
        move_along = False
        output = "Your discord account is already registered to a nation!"
    else:
        move_along = True
        output = "Your discord account is not currently linked to a nation! Would you like to register now?"

    embed = discord.Embed(
        title=title,
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    if move_along:
        check = await reaction_factory.Confirminator(ctx, message).navigate()
        await utils.clean_up(message)
        if check:
            return func_0_0, ctx
        else:
            return cancel, ctx
    else:
        await asyncio.sleep(10)
        await utils.clean_up(message)
        return None, None


async def func_0_0(ctx):
    embed = discord.Embed(
        title=title,
        description="Alright! At this time, please say the name or id of the nation you wish to register as!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    try:
        response = await ctx.bot.wait_for("message", timeout=60, check=lambda m: m.author == ctx.author)
        nation = await scrape.lookup(ctx.bot.pw.nations_latest, id=response.content, name=response.content)
        await utils.clean_up(message)
        if nation:
            return func_0_1, ctx, nation
        else:
            return try_again, ctx, "I wasn't able to find a nation matching the given input!"

    except asyncio.TimeoutError:
        await utils.clean_up(message)
        return utils.handle_timeout, ctx, func_0_0, cancel, "Requiem Nation Registration"


async def func_0_1(ctx, nation):
    async with ctx.bot.pool.acquire() as conn:
        register_check = await conn.fetchval("SELECT * FROM users WHERE nation_id = ($1)", nation.id)
    if register_check:
        return try_again, ctx, "That nation is already linked to a discord account!"
    else:
        return func_0_2, ctx, nation


async def func_0_2(ctx, nation):
    output = f"Alright! I found the nation [**{nation.name}**](https://politicsandwar.com/nation/id={nation.id})!\n" \
             "Does this look right?"
    embed = discord.Embed(
        title=title,
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    check = await reaction_factory.Confirminator(ctx, message).navigate()
    await utils.clean_up(message)
    if check:
        return func_0_3, ctx, nation
    else:
        return try_again, ctx, "The nation found wasn't what you were searching for!"


async def func_0_3(ctx, nation):
    output = "Alright! You should receive a verification code in-game within a few minutes! When you do, please" \
             " post it here!"
    embed = discord.Embed(
        title=title,
        description=output,
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    token = await send_message(ctx.bot, nation.leader, ctx.author)
    response = await ctx.bot.wait_for("message", timeout=300, check=lambda m: m.author == ctx.author)
    await utils.clean_up(message)
    if response.content == token:
        return func_0_4, ctx, nation
    else:
        return func_0_5, ctx


async def func_0_4(ctx, nation):
    async with ctx.bot.pool.acquire() as conn:
        await conn.execute("UPDATE users SET nation_id = ($1) WHERE user_id = ($2)", nation.id, ctx.author.id)
    output = "Alright! You have successfully registered your nation with Requiems Nation Registration Service!"
    embed = discord.Embed(
        title=title,
        description=output,
        colour=ctx.colour
    )
    await ctx.send(embed=embed, delete_after=15)
    return None, None


async def func_0_5(ctx):
    output = "You have provided an incorrect token! You will have to wait 24 hours before attempting to register again!"
    embed = discord.Embed(
        title=title,
        description=output,
        colour=ctx.colour
    )
    await ctx.send(embed=embed, delete_after=15)
    return None, None
