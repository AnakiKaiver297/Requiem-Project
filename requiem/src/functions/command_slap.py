# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_slap(ctx, target):
    if target:
        if target.id == ctx.bot.user.id:
            async with ctx.bot.csess.get("https://nekos.life/api/v2/img/slap") as session:
                data = await session.json(content_type="application/json")
            url = data["url"]

            embed = discord.Embed(title=f"**{ctx.bot.user.name}** slapped **{ctx.author.name}** back!",
                                  color=ctx.colour)
            embed.set_image(url=url)
            embed.set_footer(text="Results provided by https://Nekos.Life")

        elif target.id != ctx.author.id:
            async with ctx.bot.csess.get("https://nekos.life/api/v2/img/slap") as session:
                data = await session.json(content_type="application/json")
            url = data["url"]

            embed = discord.Embed(title=f"**{ctx.author.name}** slapped **{target.name}**!", color=ctx.colour)
            embed.set_image(url=url)
            embed.set_footer(text="Results provided by https://Nekos.Life")

        else:
            embed = discord.Embed(description=f"**{ctx.author.name}** proceeds to slap themselves repeatedly for no "
                                              "apparent reason! Maybe... its a fetish?", colour=ctx.colour)

    else:
        embed = discord.Embed(description="Slapping people isn't very nice, unless they're australian, in which case "
                                          "they don't exist.", colour=ctx.colour)
    await ctx.send(embed=embed)
