# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import extra_funcs
import discord
import asyncio
import time
import contextlib


async def run_f(ctx, reason):
    users = [f"**{ctx.author.name}**"]
    update = False
    rstr = "\N{REGIONAL INDICATOR SYMBOL LETTER F}"
    msg = None
    timer = 400

    if reason:
        if len(reason) > 50:
            users = [ctx.bot.user.mention]
            rea = " for the reason being too long."
        else:
            rea = f" for {reason}."
    else:
        rea = "."

    start = time.time()
    while timer > 0:

        if len(users) <= 4:
            out = f"{extra_funcs.stringify(users, 'and')} have paid their respects{rea}"
        else:
            out = f"{users[0]}, {users[1]}, {users[2]}, and {len(users) - 3} others have paid their respects{rea}"

        embed = discord.Embed(description=out, color=ctx.colour)

        if msg:
            if update:
                msg = await ctx.channel.fetch_message(msg.id)
                await msg.edit(embed=embed)
                update = False

            def check(r, u):
                return all(i for i in [r.message.id == msg.id, f"**{u.name}**" not in users and r.emoji == rstr])

            with contextlib.suppress(asyncio.TimeoutError):
                reaction, user = await ctx.bot.wait_for(
                    "reaction_add",
                    check=check,
                    timeout=timer
                )
                users.append(f"**{user.name}**")
                update = True

            elapsed = time.time() - start
            timer -= elapsed

        else:
            msg = await ctx.send(embed=embed)
            await msg.add_reaction(rstr)

        timer -= 1
        await asyncio.sleep(1)
