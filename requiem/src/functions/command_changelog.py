# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import resource_handler as resource
from __init__ import __version__


async def run_changelog(ctx):
    changelog = resource.fetch("latest")
    embed = discord.Embed(
        title=f"Requiem Version: {__version__}",
        color=ctx.colour,
        description=changelog
    )
    await ctx.send(embed=embed)
