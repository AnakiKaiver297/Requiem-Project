# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import reaction_factory


async def clean_up(message):
    """Cleans up the last message if there was one"""
    try:
        message = await message.channel.fetch_message(message.id)
        if message:
            await message.delete()
    except discord.errors.NotFound:
        pass
    except discord.errors.Forbidden:
        pass


async def runnable(starter):
    """Function for continuous function execution"""
    func, *args = starter
    while True:
        func, *args = await func(*args)
        if not func:
            return


async def handle_timeout(ctx, passed_func, failed_func, title):
    """Handles timeout exceptions"""
    embed = discord.Embed(
        title=title,
        description="You timed out! Would you like to try again?",
        color=ctx.colour
    )
    message = await ctx.send(embed=embed)

    confirminator = reaction_factory.Confirminator(ctx, message)
    confirm = await confirminator.navigate()
    await clean_up(message)

    if confirm:
        return passed_func, ctx
    else:
        return failed_func, ctx
