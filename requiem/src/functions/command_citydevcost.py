# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
import discord


async def run_citydevcost(ctx, city, infra, land):
    embed = discord.Embed(
        title=f"Developed City Calculator",
        description=f"City {city} - {infra} Infra {land} Land",
        colour=ctx.colour)
    try:
        citycost = calculate.city(city)
        infracost = calculate.infra(10, infra)
        landcost = calculate.land(250, land)
        total = citycost + infracost + landcost
        embed.add_field(name=f"City {city}", value=f"${citycost:,.2f}")
        embed.add_field(name=f"{infra} Infra", value=f"${infracost:,.2f}")
        embed.add_field(name=f"{land} Land", value=f"${landcost:,.2f}")
        embed.add_field(name="Total Cost",
                        value=f"${total:,.2f}", inline=False)
    except ValueError as error:
        embed.add_field(
            name="There was a problem with one of the values given!", value=f"{error}")
    await ctx.send(embed=embed)
