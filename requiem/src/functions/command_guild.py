# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_guild(ctx):
    guild = ctx.guild
    embed = discord.Embed(title=guild.name, color=ctx.colour)

    embed.add_field(name="Guild Owner", value=guild.owner)
    embed.add_field(name="Guild ID", value=guild.id)
    embed.add_field(name="Region", value=guild.region)
    embed.add_field(name="Server Verification Level", value=guild.verification_level)
    embed.add_field(name="Server Created On", value=guild.created_at.strftime("%A %B %d, %Y"))
    embed.add_field(name="Members", value=guild.member_count)
    embed.add_field(name="Roles", value=f"{len(guild.roles)}")
    embed.add_field(name="Text Channels", value=f"{len(guild.channels)}")
    embed.add_field(name="Voice Channels", value=f"{len(guild.voice_channels)}")
    embed.set_thumbnail(url=guild.icon_url)

    await ctx.send(embed=embed)
