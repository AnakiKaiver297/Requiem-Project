# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_hug(ctx, target):
    if target:
        if target.id == ctx.bot.user.id:
            embed = discord.Embed(description="Aww I love you too!", colour=ctx.colour)

        elif target.id != ctx.author.id:
            async with ctx.bot.csess.get("https://nekos.life/api/v2/img/hug") as session:
                data = await session.json(content_type="application/json")
            url = data["url"]

            embed = discord.Embed(title=f"**{ctx.author.name}** hugged **{target.name}**!", color=ctx.colour)
            embed.set_image(url=url)
            embed.set_footer(text="Results provided by https://Nekos.Life")

        else:
            embed = discord.Embed(description="You can't give hug yourself silly!", colour=ctx.colour)

    else:
        embed = discord.Embed(description="Everybody needs a hug now and then! Who are you going to give it to?",
                              colour=ctx.colour)
    await ctx.send(embed=embed)
