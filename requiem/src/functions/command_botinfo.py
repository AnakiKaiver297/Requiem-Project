# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import os
import discord
import platform
from core.engine.utils import resource_handler
from __init__ import __version__


def countlines(start, lines=0):

    for thing in os.listdir(start):
        thing = os.path.join(start, thing)
        if os.path.isfile(thing):
            if thing.endswith('.py'):
                with open(thing, 'r', encoding="utf8") as f:
                    newlines = f.readlines()
                    newlines = len(newlines)
                    lines += newlines

    for thing in os.listdir(start):
        thing = os.path.join(start, thing)
        if os.path.isdir(thing):
            lines = countlines(thing, lines)

    return lines


async def run_botinfo(ctx):

    mins, seconds = divmod(ctx.bot.uptime.total_seconds(), 60)
    hours, mins = divmod(mins, 60)
    uptime = f"{int(hours)} Hours {int(mins)} Minutes {int(seconds)} Seconds"

    lines = countlines(".")

    output = resource_handler.fetch("description")
    output = output.format(
        __version__, discord.__version__, platform.python_version(), uptime,
        len(ctx.bot.guilds), len(ctx.bot.users), len(ctx.bot.database.user_configs),
        len(ctx.bot.execution.served), ctx.bot.execution.executed, lines,
        ctx.loc_prefix
    )
    embed = discord.Embed(
        description=output,
        colour=ctx.colour
    )
    await ctx.send(embed=embed)
