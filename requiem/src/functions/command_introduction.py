# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import resource_handler as resource


async def run_introduction(ctx):
    message = resource.fetch("introduction")
    embed = discord.Embed(
        title="Thank You For Choosing Requiem!",
        description=message.format(ctx.loc_prefix, ctx.loc_prefix),
        color=ctx.colour
    )
    await ctx.send(embed=embed)
