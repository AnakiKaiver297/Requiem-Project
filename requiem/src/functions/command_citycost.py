# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
import discord


async def run_citycost(ctx, city):
    embed = discord.Embed(title="City Cost Calculator",
                          description=f"City {city}", colour=ctx.colour)
    try:
        price = calculate.city(city)
        embed.add_field(name="Total Cost", value=f"${price:,.2f}")
    except ValueError as error:
        embed.add_field(
            name="There was a problem with the value given!", value=f"{error}")
    await ctx.send(embed=embed)
