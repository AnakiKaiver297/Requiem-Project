
from core.engine.utils import reaction_factory, help_funcs
import discord


async def run_help(ctx, target):

    cogs_dict, commands_dict = await help_funcs.generate_dicts(ctx)
    pages = []
    prefix = ctx.loc_prefix

    if target:

        target = target.replace("'", "").lower()

        if target in cogs_dict.keys():

            cog_name, cog_help, cog_commands = cogs_dict[target]

            title = f"Help - {cog_name}"
            description = cog_help
            commands_in_page = 0

            embed = discord.Embed(title=title, description=description, colour=ctx.colour)

            for command_name in sorted(cog_commands):
                command = cog_commands[command_name]

                if not command.hidden:
                    if commands_in_page == 10:
                        pages.append(embed)
                        embed = discord.Embed(title=title, description=description, colour=ctx.colour)
                        commands_in_page = 0

                    if command.parent:
                        name = f"{command.parent.name} {command.name}"
                    else:
                        name = command.name

                    if len(command.aliases) > 0:
                        aliases = ", ".join(command.aliases)
                        name = name + f" - {aliases}"

                    brief = help_funcs.get_brief(command, prefix)
                    embed.add_field(name=name, value=brief, inline=False)
                    commands_in_page += 1

            if embed not in pages:
                pages.append(embed)

        elif target in commands_dict.keys():

            command = commands_dict[target]

            if command.parent:
                command_title = f"{command.parent.name} {command.name}"
            else:
                command_title = command.name

            help_text = help_funcs.get_help(command, prefix).replace("%prefix%", prefix)
            title = f"Help - {command_title}"
            if len(command.aliases) > 0:
                aliases = ", ".join(command.aliases)
                title = title + f" - {aliases}"
            usage = help_funcs.get_usage(command, prefix)

            embed = discord.Embed(title=title, description=help_text, colour=ctx.colour)
            embed.add_field(name="Usage", value=usage)
            pages.append(embed)

        else:

            title = "Help"
            description = f"I couldn't find a command or module with that name! See {prefix}help for a full list" \
                          f" of modules and commands!"
            embed = discord.Embed(title=title, description=description, colour=ctx.colour)
            pages.append(embed)

    else:

        title = "Help - Categories"
        description = f"Help Usage: {prefix}help [command or module name]\n" \
            f"Command Usage: {prefix}command <required argument> [optional argument]"

        cogs_in_page = 0
        embed = discord.Embed(title=title, description=description, color=ctx.colour)

        for cog_name in sorted(cogs_dict.keys()):
            cog_name, cog_help, cog_commands = cogs_dict[cog_name]

            if len(cog_commands) > 0:

                if cogs_in_page == 10:
                    pages.append(embed)
                    embed = discord.Embed(title=title, description=description, color=ctx.colour)
                    cogs_in_page = 0

                embed.add_field(name=cog_name, value=cog_help, inline=False)
                cogs_in_page += 1

        if embed not in pages:
            pages.append(embed)

    paginator = reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages)
    await paginator.navigate()
