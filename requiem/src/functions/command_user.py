# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord


async def run_user(ctx, user):
    if not user:
        user = ctx.message.author

    embed = discord.Embed(title=user.name, color=ctx.colour)

    if user.name != user.display_name:
        embed.add_field(name="Nickname:", value=user.display_name)

    embed.add_field(name="Snowflake:", value=user.id)
    embed.add_field(name="Account Created On:",
                    value=f'{user.created_at.strftime("%A %B %d, %Y")}')

    if ctx.guild:
        embed.add_field(name="Member Joined On:",
                        value=f'{user.joined_at.strftime("%A %B %d, %Y")}')
        roles = ", ".join(i.name for i in user.roles[1:]) if len(
            user.roles) > 0 else "User has no roles!"
        embed.add_field(name="Roles", value=roles, inline=False)

    if user.activity:
        if user.activity.type == 4:
            embed.add_field(name="Custom Status",
                            value="DPY does not currently provide a method for getting custom statuses!")
        else:
            embed.add_field(name=user.activity.type.name.title(),
                            value=user.activity.name)

    if str(user.status) == "dnd":
        status = "Do Not Disturb"
    else:
        status = str(user.status).title()

    embed.add_field(name="**Status:**", value=status)
    embed.set_thumbnail(url=user.avatar_url)
    await ctx.send(embed=embed)
