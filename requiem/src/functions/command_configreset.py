# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from core.engine.utils import reaction_factory


async def run_configreset(ctx):
    embed = discord.Embed(
        title="Clear Guild Config",
        description="Are you sure you wish to clear this guilds config? This action is not reversible!",
        colour=ctx.colour
    )
    message = await ctx.send(embed=embed)
    confirm = await reaction_factory.Confirminator(ctx, message).navigate()

    if confirm:
        prefix = ctx.bot.config.prefix
        query = "UPDATE guilds SET prefix = ($1), embed_colour = ($2), greet_channel = ($3), greet_message = ($4), " \
                "greet_dm = ($5), greet_dm_message = ($6), auto_role = ($7), leave_channel = ($8), " \
                "leave_message = ($9), offensive_monitor = ($10), defensive_monitor = ($11), " \
                "applicant_monitor = ($12), beige_monitor = ($13) WHERE guild_id = ($14)"
        args = prefix, "purple", 0, None, False, None, 0, 0, None, "{}", "{}", "{}", "{}", ctx.guild.id
        async with ctx.bot.pool.acquire() as conn:
            await conn.execute(query, *args)
        output = "Alright! The configuration for this guild has been reset!"

    else:
        output = "Alright! Your configuration has not been touched!"

    embed = discord.Embed(title="Clear Guild Config", description=output, colour=ctx.colour)
    await ctx.send(embed=embed)
