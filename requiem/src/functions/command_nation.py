# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from discord.ext import commands
from core.engine.utils import extra_funcs
from pwpy import exceptions, calculate
import discord


async def run_nation(ctx, target):
    if ctx.bot.pw.nations_latest:

        s_nation, user, leader = await extra_funcs.find_nation_by_user(ctx, target)

        try:
            nation = await ctx.bot.pw.nation(s_nation.id)

            att_max, att_min, def_max, def_min = calculate.score_range(
                nation.score)

            embed = discord.Embed(
                description=f"[{nation.name}](https://politicsandwar.com/nation/id={nation.id}) - {leader}",
                colour=ctx.colour
            )
            embed.add_field(name="Score", value=f'{nation.score:,}')
            embed.add_field(name="Defensive Range",
                            value=f"{int(def_min):,} - {int(def_max):,}")
            embed.add_field(name="Offensive Range",
                            value=f"{int(att_min):,} - {int(att_max):,}")
            if nation.alliance_id != 0:
                alliance = f"[{nation.alliance}](https://politicsandwar.com/alliance/id={nation.alliance_id})"
                embed.add_field(name="Alliance", value=alliance)
            embed.add_field(name="Color", value=nation.color.title())
            embed.add_field(name="Age", value=f"{nation.daysold:,} Days Old")
            embed.add_field(name="Government", value=nation.government)
            embed.add_field(name="War Policy", value=nation.war)
            embed.add_field(name="Domestic Policy", value=nation.domestic)
            embed.add_field(
                name="Rank", value=f"{nation.rank:,} of {nation.total_nations:,} nations")
            embed.add_field(name="Cities", value=nation.cities)
            embed.add_field(name="Population", value=f'{nation.population:,}')
            embed.add_field(name="Land", value=f'{nation.land:,}')
            embed.add_field(name="Infra", value=f'{nation.infra:,}')
            if nation.beige_turns_left > 0:
                embed.add_field(name="Beige Turns Left",
                                value=nation.beige_turns_left)
            embed.add_field(name="Defensive Wars", value=nation.defensive_wars)
            embed.add_field(name="Offensive Wars", value=nation.offensive_wars)
            embed.add_field(name="Soldiers", value=f'{nation.soldiers:,}')
            embed.add_field(name="Tanks", value=f'{nation.tanks:,}')
            embed.add_field(name="Aircraft", value=f'{nation.aircraft:,}')
            embed.add_field(name="Ships", value=f'{nation.ships:,}')
            embed.add_field(name="Missiles", value=f'{nation.missiles:,}')
            embed.add_field(name="Nukes", value=f'{nation.nukes:,}')
            embed.set_image(url=nation.flag)

        except (exceptions.InvalidTarget, AttributeError):
            embed = discord.Embed(
                title="No Nation Found",
                description="I was unable to find a nation matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
        except RuntimeError:
            embed = discord.Embed(
                title="Response Read Error",
                description="The API returned a broken response! Please try again later...",
                colour=ctx.colour
            )

    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching nations! Please wait a few minutes and try again!",
            colour=ctx.colour
        )
    await ctx.send(embed=embed)
