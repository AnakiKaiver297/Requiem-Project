# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import scrape, calculate
from core.engine.utils import extra_funcs
import discord
import json


def add_nation_to_embed(bot, guild, embed, status, nation, alliance, military):
    att_max, att_min, def_max, def_min = calculate.score_range(nation.score)
    nation_name = f"[{nation.name}](https://politicsandwar.com/nation/id={nation.id})"
    try:
        conf = bot.database.get_user_config
        leader = [u for u in guild.members if conf(u.id) and conf(u.id).nation_id == nation.id][0].mention
    except (IndexError, AttributeError):
        leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
            f"receiver={nation.leader.replace(' ', '%20')})"
    embed.add_field(name=f"{status} Nation",
                    value=f"{nation_name} - {leader}", inline=False)
    if alliance:
        value = f"[{alliance.name}](https://politicsandwar.com/alliance/id={alliance.id})"
        if alliance.discord != "":
            value += f" - [Discord]({alliance.discord})"
        if nation.alliance_rank == 1:
            value = value + " - This Nation is an Applicant!"
        embed.add_field(name="Alliance", value=value, inline=False)
    embed.add_field(name="Score", value=f'{nation.score:,}')
    embed.add_field(
        name="War Range",
        value=f"Defensive {int(def_min):,} - {int(def_max):,}\nOffensive {int(att_min):,} - {int(att_max):,}",
        inline=False)
    embed.add_field(name="Cities", value=nation.cities)
    embed.add_field(name="War Policy", value=nation.war)
    embed.add_field(name="Soldiers", value=f'{military.soldiers:,}')
    embed.add_field(name="Tanks", value=f'{military.tanks:,}')
    embed.add_field(name="Aircraft", value=f'{military.aircraft:,}')
    embed.add_field(name="Ships", value=f'{military.ships:,}')
    embed.add_field(name="Missiles", value=f'{military.missiles:,}')
    embed.add_field(name="Nukes", value=f'{military.nukes:,}')


async def send_war_notification(bot, guild, channel, notif_role, colour, war, type_of_war, n1type, n1, a1, m1, n2type,
                                n2, a2, m2):
    if channel:
        war_type_and_id = f"[New {type_of_war} War](https://politicsandwar.com/nation/war/timeline/war={war.id})"
        if a1:
            alliance = f"[{a1.name}](https://politicsandwar.com/alliance/id={a1.id})"
        else:
            alliance = f"Solo Nation"
        embed = discord.Embed(
            description=f"{alliance} - {war_type_and_id}", colour=colour)
        add_nation_to_embed(bot, guild, embed, n1type, n1, a1, m1)
        add_nation_to_embed(bot, guild, embed, n2type, n2, a2, m2)

        try:
            if notif_role:
                await channel.send(notif_role.mention)
            await channel.send(embed=embed)
        except (discord.Forbidden, discord.HTTPException):
            pass


async def run_war_monitor(self, war):
    pnw = self.bot.pw
    bot = self.bot

    if not all(i for i in [pnw.militaries_latest, pnw.nations_latest, pnw.alliances_latest]):
        return

    attacker = await scrape.lookup(pnw.nations_latest, id=war.attacker)
    defender = await scrape.lookup(pnw.nations_latest, id=war.defender)

    if not attacker or not defender:
        return

    attacker_mil = await scrape.lookup(pnw.militaries_latest, nation_id=war.attacker)
    attacker_aa = await scrape.lookup(pnw.alliances_latest, id=attacker.alliance_id)
    defender_mil = await scrape.lookup(pnw.militaries_latest, nation_id=war.defender)
    defender_aa = await scrape.lookup(pnw.alliances_latest, id=defender.alliance_id)

    if all(i for i in [attacker, attacker_mil, attacker_aa, defender, defender_aa, defender_mil]):

        for guild in self.bot.guilds:
            config = self.bot.get_guild_config(guild.id)

            if config:
                colour = extra_funcs.get_colour(config.embed_colour)
                off_wars = json.loads(
                    config.offensive_monitor.replace("'", '"'))
                if str(attacker.alliance_id) in off_wars.keys():
                    channel, role = off_wars[str(
                        attacker.alliance_id)].values()
                    role = discord.utils.get(guild.roles, id=role)
                    channel = discord.utils.get(guild.channels, id=channel)
                    await send_war_notification(bot, guild, channel, role, colour, war, "Offensive", "Attacking",
                                                attacker,  attacker_aa, attacker_mil, "Defending", defender,
                                                defender_aa, defender_mil)

                def_wars = json.loads(config.defensive_monitor)
                if str(defender.alliance_id) in def_wars.keys():
                    channel, role = def_wars[str(
                        defender.alliance_id)].values()
                    role = discord.utils.get(guild.roles, id=role)
                    channel = discord.utils.get(guild.channels, id=channel)
                    await send_war_notification(bot, guild, channel, role, colour, war, "Defensive", "Defending",
                                                defender, defender_aa, defender_mil, "Attacking", attacker,
                                                attacker_aa, attacker_mil)

        for user in bot.users:
            config = self.bot.get_user_config(user.id)
            if config:
                if config.war_alerts:
                    colour = extra_funcs.get_colour("purple")
                    if config.nation_id == defender.id:
                        await send_war_notification(bot, None, user, None, colour, war, "Defensive", "Defending",
                                                    defender, defender_aa, defender_mil, "Attacking", attacker,
                                                    attacker_aa, attacker_mil)
