# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import exceptions, scrape
from core.engine.utils import reaction_factory, extra_funcs
import discord
import asyncio
from discord.ext import commands


async def run_findraids(ctx, target):

    target = target if target else ""

    pages = []

    if ctx.bot.pw.nations_latest and ctx.bot.pw.alliances_latest and ctx.bot.pw.militaries_latest:

        green = True
        discriminate = None

        if "-alliance" in target:
            target_alliance = target.split("-alliance", 1)[1][1:]
            target = target.split("-alliance", 1)[0][:-1]
            alliance = await scrape.lookup(ctx.bot.pw.alliances_latest, id=target_alliance, name=target_alliance)
            if alliance:
                discriminate = alliance.id
            else:
                green = False

        elif "-no_alliances" in target:
            target = target.split("-no_alliances", 1)[0][:-1]
            discriminate = 0

        if green:

            raider, user, leader = await extra_funcs.find_nation_by_user(ctx, target)

            if raider:
                try:
                    async with ctx.typing():
                        targets = await scrape.get_raid_targets(
                            ctx.bot.pw.nations_latest, raider, discriminate=discriminate)
                        count = 0

                        for nation in targets:
                            count += 1
                            try:
                                conf = ctx.get_user_config
                                leader = [u for u in ctx.bot.users if conf(
                                    u.id).nation_id == nation.id][0].mention
                            except (IndexError, AttributeError):
                                leader = f"[{nation.leader}](https://politicsandwar.com/inbox/message/" \
                                    f"receiver={nation.leader.replace(' ', '%20')})"
                            military = await scrape.lookup(ctx.bot.pw.militaries_latest, nation_id=nation.id)
                            alliance = await scrape.lookup(ctx.bot.pw.alliances_latest, id=nation.alliance_id)
                            embed = discord.Embed(
                                title=f"Raid Target - {count} of {len(targets)}",
                                description=f"[{nation.name}](https://politicsandwar.com/nation/id={nation.id}) - {leader}",
                                colour=ctx.colour
                            )
                            if alliance:
                                value = f"[{alliance.name}](https://politicsandwar.com/alliance/id={alliance.id})"
                                if alliance.discord != "":
                                    value += f" - [Discord]({alliance.discord})"
                                embed.add_field(
                                    name="Alliance", value=value, inline=False)
                            embed.add_field(name="Score", value=nation.score)
                            embed.add_field(
                                name="Cities", value=f'{nation.cities:,.2f}')
                            embed.add_field(name="War Policy",
                                            value=nation.war)
                            embed.add_field(name="Soldiers",
                                            value=f'{military.soldiers:,.2f}')
                            embed.add_field(
                                name="Tanks", value=f'{military.tanks:,.2f}')
                            embed.add_field(name="Aircraft",
                                            value=f'{military.aircraft:,.2f}')
                            embed.add_field(
                                name="Ships", value=f'{military.ships:,.2f}')
                            embed.add_field(name="Missiles",
                                            value=f'{military.missiles:,.2f}')
                            embed.add_field(
                                name="Nukes", value=f'{military.nukes:,.2f}')
                            await asyncio.sleep(.01)
                            pages.append(embed)

                except ValueError:
                    embed = discord.Embed(
                        title="Impossible Raid Alliance",
                        description="Nations cannot raid alliances that they are a member of!",
                        colour=ctx.colour
                    )
                    pages.append(embed)

                except IndexError:
                    embed = discord.Embed(
                        title="No Raids Available",
                        description="There are no raids presently available for this nation!",
                        colour=ctx.colour
                    )
                    pages.append(embed)

            else:
                embed = discord.Embed(
                    title="No Nation Found",
                    description="I was unable to find a nation matching the given input!"
                                " Please check your input and try again!",
                    colour=ctx.colour
                )
                pages.append(embed)
        else:
            embed = discord.Embed(
                title="No Alliance Found",
                description="I was unable to find an alliance matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
            pages.append(embed)

    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching nations, alliances, and militaries! "
                        "Please wait a few minutes and try again!",
            colour=ctx.colour
        )
        pages.append(embed)

    paginator = reaction_factory.Paginator(ctx.bot, ctx, ctx.author, pages)
    await paginator.navigate()
