# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from pwpy import calculate
import discord


async def run_land(ctx, start, end):
    try:
        value = calculate.land(start, end)

        embed = discord.Embed(
            title="Land Calculator",
            description=f"From {start} to {end} Land",
            colour=ctx.colour
        )
        embed.add_field(name="Starting Land Amount",
                        value=f"{start:,.2f}", inline=True)
        embed.add_field(name="Finishing Land Amount",
                        value=f"{end:,.2f}", inline=True)
        embed.add_field(name="Total Cost",
                        value=f"${value:,.2f}", inline=False)

    except ValueError:
        embed = discord.Embed(
            title="Land Calculator",
            description=f"You passed an invalid value! Please note that this calculator is limited to a max of 10000 "
                        f"land and can not calculate land delete payback!",
            colour=ctx.colour
        )

    await ctx.send(embed=embed)
