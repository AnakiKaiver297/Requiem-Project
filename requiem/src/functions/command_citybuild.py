# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from pwpy import exceptions


async def run_citybuild(ctx, city):
    if ctx.bot.pw.cities_latest:
        try:
            city = await ctx.bot.pw.city(city)
            totalimps = city.coalpwr + city.oilpwr + city.nucpwr + city.windpwr + city.coalmne + city.oilwell + \
                city.ironmne + city.baumne + city.leadmne + city.uramne + city.farm + city.gasref + city.steelmll + \
                city.aluref + city.munfac + city.polstat + city.hospital + city.reccenter + city.subway + \
                city.sprmarket + city.bank + city.mall + city.stadium + city.barracks + city.factory + city.hangar + \
                city.drydock

            city_template = f"""
VVV
    "infra_needed": {totalimps * 50},
    "imp_total": {totalimps},
    "imp_coalpower": {city.coalpwr},
    "imp_oilpower": {city.oilpwr},
    "imp_windpower": {city.windpwr},
    "imp_nuclearpower": {city.nucpwr},
    "imp_coalmine": {city.coalmne},
    "imp_oilwell": {city.oilwell},
    "imp_uramine": {city.uramne},
    "imp_leadmine": {city.leadmne},
    "imp_ironmine": {city.ironmne},
    "imp_bauxitemine": {city.baumne},
    "imp_farm": {city.farm},
    "imp_gasrefinery": {city.gasref},
    "imp_aluminumrefinery": {city.aluref},
    "imp_munitionsfactory": {city.munfac},
    "imp_steelmill": {city.steelmll},
    "imp_policestation": {city.polstat},
    "imp_hospital": {city.hospital},
    "imp_recyclingcenter": {city.reccenter},
    "imp_subway": {city.subway},
    "imp_supermarket": {city.sprmarket},
    "imp_bank": {city.bank},
    "imp_mall": {city.mall},
    "imp_stadium": {city.stadium},
    "imp_barracks": {city.barracks},
    "imp_factory": {city.factory},
    "imp_hangars": {city.hangar},
    "imp_drydock": {city.drydock}
DDD
""".replace("VVV", "{").replace("DDD", "}")
            embed = discord.Embed(
                title="City JSON Export",
                description=f"```{city_template}```",
                colour=ctx.colour
            )
        except exceptions.InvalidTarget:
            embed = discord.Embed(
                title="No City Found",
                description="I was unable to find a city matching the given input!"
                            " Please check your input and try again!",
                colour=ctx.colour
            )
        except RuntimeError:
            embed = discord.Embed(
                title="Response Read Error",
                description="The API returned a broken response! Please try again later...",
                colour=ctx.colour
            )
    else:
        embed = discord.Embed(
            title="PWPY is not ready yet!",
            description="PWPY has not finished caching cities! Please wait a few minutes and try again!",
            colour=ctx.colour
        )
    await ctx.send(embed=embed)
