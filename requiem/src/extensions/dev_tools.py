# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
from core.engine.utils import reaction_factory, resource_handler as resource
import discord
import contextlib
import traceback
import inspect
import textwrap
from contextlib import redirect_stdout
import io


class DevTools(objects.Cog):
    """Developer commands to make developing and debugging easier"""

    def __init__(self, bot):
        self.bot = bot
        self.name = "Dev Tools"
        self._last_result = None
        self.sessions = set()

    async def cog_check(self, ctx):
        """Checks the author id against the list of owners and determines
        if the command should run or not."""
        bot = ctx.bot
        if ctx.author.id in bot.config.owners:
            return True
        else:
            raise commands.NotOwner("You do not own this bot!")

    @commands.command(brief="Enable/disable a specified command.", ignore_extra=False)
    async def togglecmd(self, ctx, *, command):
        """Enable/disable a specified command for the remainder of this runtime instance.
        ```
        Parameters
        ------------
        command: <Required>
            The command to be disabled/enabled for the remainder of the active session.
        ```
        """
        command = ctx.bot.get_command(command)
        if command:
            if command.enabled:
                command.update(enabled=False)
                toggle = "disabled"
            else:
                command.update(enabled=True)
                toggle = "enabled"
            output = f"Alright, the command {command.name} has been {toggle}! " \
                "It will remain disabled until the bot is restarted or it is re-enabled!"
        else:
            output = "No such command could be found!"
        embed = discord.Embed(title="Command Toggle",
                              description=output, colour=discord.Colour.blue())
        await ctx.send(embed=embed)

    @commands.command(brief="Get an exceptions logs using exception code.", ignore_extra=False)
    async def exception(self, ctx, exception_code: int):
        """Request an exception dump by providing a matching exception code.
        ```
        Parameters
        ------------
        exception code: <Required>
            The exception code to lookup in Requiems errors database.
        ```
        """
        async with self.bot.pool.acquire() as conn:
            traceback, context, occurrences = await conn.fetchrow(
                "SELECT traceback, context, occurrences FROM exceptions WHERE reference = ($1)", exception_code)
        output = f"{context}\nException---\n{traceback}"
        pages = reaction_factory.build_embed_pages(
            output, length=50, color=discord.Colour.blue())
        paginator = reaction_factory.Paginator(
            self.bot, ctx, ctx.author, pages)
        await paginator.navigate()

    @commands.command(brief="Restarts the bot.", ignore_extra=False)
    async def reboot(self, ctx):
        """Completely terminates the bots service and starts a new one."""
        with contextlib.suppress(Exception):
            await ctx.message.add_reaction('✅')
        await self.bot.restart()

    @commands.command(brief="Terminates the bot.", ignore_extra=False)
    async def terminate(self, ctx):
        """Completely terminates the bots service."""
        with contextlib.suppress(Exception):
            await ctx.message.add_reaction('✅')
        await self.bot.terminate()

    @commands.command(brief="Raises an exception.")
    async def error(self, ctx):
        """Raises an exception. Useful for testing the error handler."""
        raise Exception

    @commands.command(brief="Loads an extension.", ignore_extra=False)
    async def load(self, ctx, *, extension):
        """Loads the specified extension into the bot so it can be used.
        ```
        Parameters
        ------------
        extension: <Required>
            The extension to load into memory.
        ```
        """
        extension = extension.replace(" ", "_")
        result = self.bot.load_extension(extension)
        embed = discord.Embed(
            title="Load Extension",
            description=result,
            color=discord.Colour.blue()
        )
        await ctx.send(embed=embed)

    @commands.command(brief="Unloads an extension.", ignore_extra=False)
    async def unload(self, ctx, *, extension):
        """Unloads the specified extension. Unloaded extensions can not be interacted with.
        ```
        Parameters
        ------------
        extension: <Required>
            The extension to remove from memory.
        ```
        """
        extension = extension.replace(" ", "_")
        result = self.bot.unload_extension(extension)
        embed = discord.Embed(
            title="Unload Extension",
            description=result,
            color=discord.Colour.blue()
        )
        await ctx.send(embed=embed)

    @commands.command(brief="Reloads an extension.", ignore_extra=False)
    async def reload(self, ctx, *, extension):
        """Reloads an extension.
        ```
        Parameters
        ------------
        extension: <Required>
            The extension to reload into memory.
        ```
        """
        extension = extension.replace(" ", "_")
        r1 = self.bot.unload_extension(extension)
        r2 = self.bot.load_extension(extension)
        result = r1 + "\n" + r2
        embed = discord.Embed(
            title="Reload Extension",
            description=result,
            color=discord.Colour.blue()
        )
        await ctx.send(embed=embed)

    @staticmethod
    def cleanup_code(content):
        """Automatically removes code blocks from the code."""
        # remove ```py\n```
        if content.startswith('```') and content.endswith('```'):
            return '\n'.join(content.split('\n')[1:-1])

        # remove `foo`
        return content.strip('` \n')

    @staticmethod
    def get_syntax_error(e):
        if e.text is None:
            return '```py\n{0.__class__.__name__}: {0}\n```'.format(e)
        return '```py\n{0.text}{1:>{0.offset}}\n{2}: {0}```'.format(e, '^', type(e).__name__)

    @commands.command(name='exec', brief="Execute provided code on demand.")
    async def _eval(self, ctx, *, body: str):
        """Executes given code in the current channel.
        ```
        Parameters
        ------------
        body: <Required>
            The code to be executed.
        ```
        """
        env = {
            'bot': self.bot,
            'ctx': ctx,
            'channel': ctx.message.channel,
            'author': ctx.message.author,
            'server': ctx.message.guild,
            'message': ctx.message,
            '_': self._last_result
        }

        env.update(globals())

        body = self.cleanup_code(body)
        stdout = io.StringIO()

        to_compile = 'async def func():\n%s' % textwrap.indent(body, '  ')

        try:
            exec(to_compile, env)
        except SyntaxError as e:
            return await ctx.send(self.get_syntax_error(e))

        func = env['func']
        try:
            with redirect_stdout(stdout):
                ret = await func()
        except Exception as e:
            value = stdout.getvalue()
            await ctx.send('```py\n{}{}\n```'.format(value, traceback.format_exc()))
            reaction = '❎'
        else:
            value = stdout.getvalue()

            if ret is None:
                if value:
                    await ctx.send('```py\n%s\n```' % value)
            else:
                self._last_result = ret
                await ctx.send('```py\n%s%s\n```' % (value, ret))
            reaction = '✅'

        if ctx.guild:
            me = ctx.guild.get_member(ctx.bot.user.id)
            if ctx.channel.permissions_for(me).add_reactions:
                await ctx.message.add_reaction(reaction)

    @commands.command(brief="Creates a REPL session.", ignore_extra=True)
    async def repl(self, ctx):
        """Creates a session in the current channel within which code can be executed."""
        msg = ctx.message

        variables = {
            'ctx': ctx,
            'bot': self.bot,
            'message': msg,
            'server': msg.guild,
            'channel': msg.channel,
            'author': msg.author,
            '_': None,
        }

        if msg.channel.id in self.sessions:
            await ctx.send('Already running a REPL session in this channel. Exit it with `quit`.')
            return

        self.sessions.add(msg.channel.id)
        await ctx.send('Enter code to execute or evaluate. `exit()` or `quit` to exit.')
        while True:
            response = await self.bot.wait_for("message", check=lambda m: m.content.startswith('`') and m.author == ctx.author)

            cleaned = self.cleanup_code(response.content)

            if cleaned in ('quit', 'exit', 'exit()'):
                await ctx.send('Exiting.')
                self.sessions.remove(msg.channel.id)
                return

            executor = exec
            if cleaned.count('\n') == 0:
                # single statement, potentially 'eval'
                try:
                    code = compile(cleaned, '<repl session>', 'eval')
                except SyntaxError:
                    pass
                else:
                    executor = eval

            if executor is exec:
                try:
                    code = compile(cleaned, '<repl session>', 'exec')
                except SyntaxError as e:
                    await ctx.send(self.get_syntax_error(e))
                    continue

            variables['message'] = response

            fmt = None
            stdout = io.StringIO()

            try:
                with redirect_stdout(stdout):
                    result = executor(code, variables)
                    if inspect.isawaitable(result):
                        result = await result
            except Exception as e:
                value = stdout.getvalue()
                fmt = '```py\n{}{}\n```'.format(value, traceback.format_exc())
            else:
                value = stdout.getvalue()
                if result is not None:
                    fmt = '```py\n{}{}\n```'.format(value, result)
                    variables['_'] = result
                elif value:
                    fmt = '```py\n{}\n```'.format(value)

            try:
                if fmt is not None:
                    if len(fmt) > 2000:
                        await ctx.send('Content too big to be printed.')
                    else:
                        await ctx.send(fmt)
            except discord.Forbidden:
                pass
            except discord.HTTPException as e:
                await ctx.send('Unexpected error: `{}`'.format(e))


def setup(bot):
    bot.add_cog(DevTools(bot))
