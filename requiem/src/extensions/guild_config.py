
# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
from src.functions import (command_greet, command_leave, command_autorole, command_prefix, command_setcolour,
                           command_configreset, cmdgrp_tag)


class GuildConfig(objects.Cog):
    """Guild specific customization commands"""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Guild Config"

    async def cog_check(self, ctx):
        if ctx.guild:
            return True
        else:
            raise commands.NoPrivateMessage("This command must be run in a guild!")

    @commands.command(aliases=["setcolor"], brief="Sets embed color/colour for this guild.")
    @commands.has_permissions(manage_guild=True)
    async def setcolour(self, ctx, colour=None):
        """Sets the color/colour that requiem uses for embedded messages in this guild.
        ```
        Parameters
        ------------
        colour: [Optional]
            The colour to be set for this guild.
            Leave blank to reset to default.
        ```
        """
        await command_setcolour.run_setcolour(ctx, colour)

    @commands.command(brief="Configure welcoming service for this guild.", ignore_extra=False)
    @commands.has_permissions(manage_guild=True)
    async def greet(self, ctx):
        """Configure the greeting service for this guild."""
        await command_greet.run_greet(ctx)

    @commands.command(brief="Configure farewell service for this guild.", ignore_extra=False)
    @commands.has_permissions(manage_guild=True)
    async def leave(self, ctx):
        """Configure the farewell service for this guild."""
        await command_leave.run_leave(ctx)

    @commands.command(brief="Configures this guilds prefix.", ignore_extra=False)
    @commands.has_permissions(manage_guild=True)
    async def prefix(self, ctx, prefix: str = None):
        """Configures this guilds prefix. Must be 4 characters or less. Not passing a prefix
        will result in it being set to default.
        ```
        Parameters
        ------------
        prefix: [Optional]
            The prefix to be set for this guild.
            Leave blank to reset to default.
        ```
        """
        await command_prefix.run_prefix(ctx, prefix)

    @commands.command(brief="Configure automatic role assignment on this guild.", ignore_extra=False, aliases=["ar"])
    @commands.has_permissions(manage_roles=True)
    @commands.bot_has_permissions(manage_roles=True)
    async def autorole(self, ctx):
        """Configures automatic role assignment on this guild."""
        await command_autorole.run_autorole(ctx)

    @commands.command(brief="Completely resets this guilds configuration.", ignore_extra=False)
    @commands.has_permissions(manage_guild=True)
    async def configreset(self, ctx):
        """Completely resets this guilds configuration.
        ```
        !Warning!
            This will completely wipe this guilds configuration!
            This action is not reversible!
        ```
        """
        await command_configreset.run_configreset(ctx)

    @commands.group(brief="Lists all available tags.", ignore_extra=False)
    @commands.has_permissions(manage_guild=True)
    async def tags(self, ctx):
        """Lists all available tags."""
        if not ctx.invoked_subcommand:
            await cmdgrp_tag.run_tag(ctx)

    @tags.group(brief="Creates a new tag.", ignore_extra=False, usage='"<phrase>" <response>')
    async def create(self, ctx, *, phrase):
        """Create a new tag.
        ```
        Parameters
        ------------
        phrase: <required>
            The phase to trigger the tag. 
            Your phrase must be encased in quotes.
        response: <required>
            The response to be given upon 
            the keyword tag being said.
        ```
        """
        await cmdgrp_tag.run_tag_create(ctx, phrase)

    @tags.group(brief="Deletes a tag.", ignore_extra=False)
    async def delete(self, ctx, *, tag):
        """Deletes a tag.
        ```
        Parameters
        ------------
        tag: <required>
            The tag to delete.
        ```
        """
        await cmdgrp_tag.run_tag_delete(ctx, tag)


def setup(bot):
    bot.add_cog(GuildConfig(bot))
