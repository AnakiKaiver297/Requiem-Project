# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from src.functions import event_announce_member_join, event_announce_member_left, event_automatic_role_assignment, event_react_to_tag


class EventHandler(objects.Cog):
    """! Background Event Handler for discord.py"""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Event Handler"
        self.bot.add_listener(self.announce_member_join, "on_member_join")
        self.bot.add_listener(self.automatic_role_assignment, "on_member_join")
        self.bot.add_listener(self.announce_member_left, "on_member_remove")
        self.bot.add_listener(self.react_to_tag, "on_message")

    async def announce_member_join(self, member):
        await event_announce_member_join.run_announce_member_join(self, member)

    async def announce_member_left(self, member):
        await event_announce_member_left.run_announce_member_left(self, member)

    async def automatic_role_assignment(self, member):
        await event_automatic_role_assignment.run_assign_role_member_join(self, member)

    async def react_to_tag(self, message):
        await event_react_to_tag.run_react_to_tag(self, message)


def setup(bot):
    bot.add_cog(EventHandler(bot))
