# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
from src.functions import command_dirtyneko
import discord


class VerinsDungeon(objects.Cog):
    """NSFW Commands. Can only be executed in NSFW channels."""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Verins Dungeon"

    @commands.command(brief="Sends a dirty neko picture.", ignore_extra=False)
    @commands.is_nsfw()
    @commands.cooldown(1, 5, type=commands.BucketType.user)
    async def dirtyneko(self, ctx):
        """Sends a dirty neko picture from nekos.life"""
        await command_dirtyneko.run_dirtyneko(ctx)



def setup(bot):
    bot.add_cog(VerinsDungeon(bot))
