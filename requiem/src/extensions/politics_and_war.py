# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
from src.functions import (command_nation, command_alliance, command_infra, command_land, command_war, command_citycost,
                           command_citydevcost, command_bulkcitycost, command_bulkcitydevcost, command_alliances,
                           command_registerpw, command_findraids, command_alliancetier, command_pwnsconfig,
                           command_citybuild, command_tradeprices, command_pwping)


class PoliticsAndWar(objects.Cog):
    """Politics and War informational tools"""

    def __init__(self, bot):
        self.bot = bot
        self.name = "Politics and War"

    async def cog_check(self, ctx):
        if hasattr(self.bot, "pw"):
            return True
        return False

    @commands.command(brief="Tests PnW connection times.", ignore_extra=False)
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def pwping(self, ctx):
        """Tests the connection times to the politics and war server."""
        await command_pwping.run_pwping(ctx)

    @commands.command(brief="Retrieves the current trade market prices.", ignore_extra=False, aliases=["tp"])
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def tradeprices(self, ctx):
        """Retrieves the current trade market prices."""
        await command_tradeprices.run_tradeprices(ctx)

    @commands.command(brief="Configure PWNS services.", ignore_extra=False, aliases=["pwns"])
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def pwnsconfig(self, ctx):
        """Configure the Politics and War Notification Service.
        Can be used to configure guild side services or personal
        services if your discord account is linked to your nation."""
        await command_pwnsconfig.run_pwnsconfig(ctx)

    @commands.command(brief="Retrieves a nations info from the API.", ignore_extra=False, aliases=["ni"])
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def nation(self, ctx, *, nation=None):
        """Retrieves a nations information from the PW API.
        ```
        Parameters
        ------------
        nation: [Optional]
            The target nation you wish to view. 
            This can be a nation name, id, or user 
            mention. Note that a user must be registered 
            with Requiem in order to be viewed via user
            mention.
        ```
        """
        await command_nation.run_nation(ctx, nation)

    @commands.command(brief="Retrieves an alliances info from the API.", ignore_extra=False, aliases=["ai"])
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def alliance(self, ctx, *, alliance=None):
        """Retrieves an alliances information from the PW API.
        ```
        Parameters
        ------------
        alliance: [Optional]
            The target alliance you wish to view.
            This can be an alliance name or id.
        ```
        """
        await command_alliance.run_alliance(ctx, alliance)

    @commands.command(brief="Retrieves an alliances info from the API.", ignore_extra=False, aliases=["laa"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def alliances(self, ctx):
        """Retrieves a list of the top 100 alliances."""
        await command_alliances.run_alliances(ctx)

    @commands.command(brief="Retrieves a wars info from the API.", ignore_extra=False, aliases=["wi"])
    @commands.cooldown(1, 10, type=commands.BucketType.user)
    async def war(self, ctx, *, war):
        """Retrieves a wars information from the PW API.
        ```
        Parameters
        ------------
        war: <Required>
            The target war you wish to view. 
            This must be a war id.
        ```
        """
        await command_war.run_war(ctx, war)

    @commands.command(brief="Calculates the cost of infra.", ignore_extra=False, aliases=["ic"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def infracost(self, ctx, start: int, end: int, *, args=""):
        """Calculates the cost of infra using a given start value and
        a desired ending value.
        ```
        Parameters
        ------------
        starting infra: <Required>
            The current amount of infra.

        ending infra: <Required>
            The amount of infra to end with.

        addional args: [Optional]
            Pass urban here if you have the 
            urbanization policy active. Pass cce 
            here if you possess the Centers for 
            Civil Engineering Project.

        Example
        ------------
        %prefix%infracost 1500 2000 urban cce
        ```
        """
        await command_infra.run_infra(ctx, start, end, args)

    @commands.command(brief="Calculates the cost of land.", ignore_extra=False, aliases=["lc"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def landcost(self, ctx, start: int, end: int):
        """Calculates the cost of land using a given start value and a
        desired ending value.
        ```
        Parameters
        ------------
        starting land: <Required>
            The current amount of land.

        ending land: <Required>
            The amount of land to end with.
        ```
        """
        await command_land.run_land(ctx, start, end)

    @commands.command(brief="Calculates the cost of city.", ignore_extra=False, aliases=["cc"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def citycost(self, ctx, city: int):
        """Calculates the cost of a city using a given desired city value.
        ```
        Parameters
        ------------
        city: <Required>
            The city to be purchased.
        ```
        """
        await command_citycost.run_citycost(ctx, city)

    @commands.command(brief="Calculates the cost of a developed city.", ignore_extra=False, aliases=["cdc"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def citydevcost(self, ctx, city: int, infra: int, land: int):
        """Calculates the cost of a developed city using given desired city,
        infra, and land values.
        ```
        Parameters
        ------------
        city: <Required>
            The city to be purchased.

        infra: <Required>
            The desired infra level.

        land: <Required>
            The desired land level.
        ```
        """
        await command_citydevcost.run_citydevcost(ctx, city, infra, land)

    @commands.command(brief="Calculates the cost of several cities.", ignore_extra=False, aliases=["bcc"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def bulkcitycost(self, ctx, citystart: int, cityend: int):
        """Calculates the cost of all cities within a given start and end value.
        ```
        Parameters
        ------------
        city start: <Required>
            The current city amount.

        city end: <Required>
            The city to purchase to.
        ```
        """
        await command_bulkcitycost.run_bulkcitycost(ctx, citystart, cityend)

    @commands.command(brief="Calculates the cost of several cities.", ignore_extra=False, aliases=["bcdc"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def bulkcitydevcost(self, ctx, citystart: int, cityend: int, infra: int, land: int):
        """Calculates the cost of all developed cities within a given start and end
        value as well as up to specified infra and land values.
        ```
        Parameters
        ------------
        city start: <Required>
            The current city amount.

        city end: <Required>
            The city to purchase to.

        infra: <Required>
            The desired infra level.

        land: <Required>
            The desired land level.
        ```
        """
        await command_bulkcitydevcost.run_bulkcitydevcost(ctx, citystart, cityend, infra, land)

    @commands.command(brief="Searches for raid targets for a given nation", ignore_extra=False, aliases=["fr"],
                      usage="<target_nation> [args]")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def findraids(self, ctx, *, target=None):
        """Searches for raid targets for a given nation id. You can pass no alliances
        or an alliance name to limit raiding targets to your liking.
        ```
        Parameters
        ------------
        nation: <Required>
            The nation to be looked up. 
            This can be a nation name or id.

        additional args: [Optional]
            Pass -alliance with an alliance 
            name after to specify a target alliance.
            Pass -no_alliances to only be 
            given targets outside of an alliance.

        Example
        ------------
        %prefix%findraids Libaria -alliance The Knights Radiant
        ```
        """
        await command_findraids.run_findraids(ctx, target)

    @commands.command(brief="Builds a list of the city tiering for a given alliance",
                      ignore_extra=False, aliases=["at"], usage="<target_alliance> [args]")
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def alliancetier(self, ctx, *, target):
        """Builds a list of the city tiering for a given alliance. Passing -tier after the
        given alliance name or id will allow you to also specify a tier value.
        Specifying a tier value will give you hyperlinks to nations in the specified tier.
        ```
        Parameters
        ------------
        alliance: <Required>
            The alliance to be looked up. 
            This can be an alliance name or id.

        additional args: [Optional]
            Pass -tier with a city tier after 
            to specify a target tier. The bot
            will return links sorted by score 
            to all nations within that tier.

        Example
        ------------
        %prefix%findraids The Knights Radiant -tier 15
        ```
        """
        await command_alliancetier.run_alliancetier(ctx, target)

    @commands.command(brief="Provides the json export for a city build.", ignore_extra=False,
                      aliases=["cb", "cityexport"])
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def citybuild(self, ctx, *, city):
        """Provides the json export for a city build.
        ```
        Parameters
        ------------
        city: <Required>
            The city to be looked up. 
            This can be a city name or id.
        ```
        """
        await command_citybuild.run_citybuild(ctx, city)

    @commands.command(brief="Registers your nation with Requiem", ignore_extra=False, aliases=["rpw"])
    @commands.cooldown(1, 86400, type=commands.BucketType.user)
    async def registerpw(self, ctx):
        """Registers yours discord account to a nation in Politics and War.
        Enables a few additional features to enhance your PW experience!"""
        await command_registerpw.run_registerpw(ctx)


def setup(bot):
    bot.add_cog(PoliticsAndWar(bot))
