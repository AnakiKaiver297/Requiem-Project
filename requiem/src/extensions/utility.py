# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
import discord
from src.functions import (command_botinfo, command_ping, command_user, command_guild, command_avatar,
                           command_changelog, command_botinvite, command_repo, command_introduction)


class Utility(objects.Cog):
    """Various utility and informational commands"""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Utility"

    @commands.command(brief="Provides additional information about Requiem.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def botinfo(self, ctx):
        """Provides information about Requiem"""
        await command_botinfo.run_botinfo(ctx)

    @commands.command(brief="Displays the bots latency.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def ping(self, ctx):
        """Displays the bot's latency."""
        await command_ping.run_ping(ctx)

    @commands.command(brief="Provides information on the specified user.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def user(self, ctx, user: discord.Member = None):
        """Provides information about a specified user.
        ```
        Parameters
        ------------
        user: <Required>
            The user to be looked up. 
            This can be a user name, mention, or id.
        ```
        """
        await command_user.run_user(ctx, user)

    @commands.command(aliases=["server"], brief="Provides information about this guild.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    @commands.guild_only()
    async def guild(self, ctx):
        """Provides information about this guild."""
        await command_guild.run_guild(ctx)

    @commands.command(brief="Gives avatar image for the specified user.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def avatar(self, ctx, user: discord.Member = None):
        """Gives avatar image for the specified user.
        ```
        Parameters
        ------------
        user: <Required>
            The user to be looked up. 
            This can be a user name, mention, or id.
        ```
        """
        await command_avatar.run_avatar(ctx, user)

    @commands.command(brief="Lists changes in the lastest patch.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def changelog(self, ctx):
        """Provides the information about the latest patch."""
        await command_changelog.run_changelog(ctx)

    @commands.command(brief="Provides a bot invite link.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def botinvite(self, ctx):
        """Provides a link that can be used to add the bot to a guild!
        In order to add bots to a guild, you must have manage_guild permissions!"""
        await command_botinvite.run_botinvite(ctx)

    @commands.command(brief="Provides the link to my github repository.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def repo(self, ctx):
        """Provides the link to my gitlab repository!"""
        await command_repo.run_repo(ctx)

    @commands.command(brief="Sends the bots introduction message again.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def introduction(self, ctx):
        """Provides the introduction message requiem sends when it joins the server!"""
        await command_introduction.run_introduction(ctx)


def setup(bot):
    bot.add_cog(Utility(bot))
