# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


from core.engine.utils import objects
from discord.ext import commands
import discord
from src.functions import (command_catfact, command_randomdoggo, command_f, command_neko, command_chucknorris,
                           command_numberfact, command_uselessfact, command_trbmb, command_randomfox, command_geekjoke,
                           command_headpat, command_hug, command_cuddle, command_tickle, command_slap, command_poke,
                           command_kiss, command_foxgirl)


class VerinsLunchbox(objects.Cog):
    """Various Meme and Joke commands"""
    def __init__(self, bot):
        self.bot = bot
        self.name = "Verins Lunchbox"

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Provides a random cat fact.", ignore_extra=False)
    async def catfact(self, ctx):
        """Provides a random cat factProvides a random cat fact."""
        await command_catfact.run_catfact(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random doggo.", ignore_extra=False)
    async def randomdoggo(self, ctx):
        """Provides a random doggo image. Prepare for ultimate adorableness."""
        await command_randomdoggo.run_randomdoggo(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random neko picture.", ignore_extra=False)
    async def neko(self, ctx):
        """Sends a random neko picture."""
        await command_neko.run_neko(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random fox girl picture.", ignore_extra=False)
    async def foxgirl(self, ctx):
        """Sends a random fox girl picture."""
        await command_foxgirl.run_foxgirl(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Cuddle with a user", ignore_extra=False)
    async def cuddle(self, ctx, user: discord.Member = None):
        """Cuddle with a user.
        ```
        Parameters
        ------------
        user: [Optional]
            The user you wish to cuddle with.
        ```
        """
        await command_cuddle.run_cuddle(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Give a head pat to a stranger in need.", ignore_extra=False)
    async def headpat(self, ctx, user: discord.Member = None):
        """Give a head pat to a stranger in need.
        ```
        Parameters
        ------------
        user: [Optional]
            The user whose head you wish to pat.
        ```
        """
        await command_headpat.run_headpat(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Give a hug to a stranger in need.", ignore_extra=False)
    async def hug(self, ctx, user: discord.Member = None):
        """Give a hug to a stranger in need.
        ```
        Parameters
        ------------
        user: [Optional]
            The user who you wish to hug.
        ```
        """
        await command_hug.run_hug(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Tickle a user.", ignore_extra=False)
    async def tickle(self, ctx, user: discord.Member = None):
        """Tickle a user.
        ```
        Parameters
        ------------
        user: [Optional]
            The user you wish to tickle.
        ```
        """
        await command_tickle.run_tickle(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Slap a user.", ignore_extra=False)
    async def slap(self, ctx, user: discord.Member = None):
        """Slap a user.
        ```
        Parameters
        ------------
        user: [Optional]
            The user who you wish to slap.
        ```
        """
        await command_slap.run_slap(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Poke a user.", ignore_extra=False)
    async def poke(self, ctx, user: discord.Member = None):
        """Poke a user.
        ```
        Parameters
        ------------
        user: [Optional]
            The user who you wish to poke.
        ```
        """
        await command_poke.run_poke(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Kiss a user.", ignore_extra=False)
    async def kiss(self, ctx, user: discord.Member = None):
        """Kiss a user.
        ```
        Parameters
        ------------
        user: [Optional]
            The user who you wish to kiss.
        ```
        """
        await command_kiss.run_kiss(ctx, user)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random useless fact.", ignore_extra=False)
    async def uselessfact(self, ctx):
        """Sends a random useless fact"""
        await command_uselessfact.run_uselessfact(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random fox.", ignore_extra=False)
    async def randomfox(self, ctx):
        """Sends a random fox"""
        await command_randomfox.run_randomfox(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Why is this a thing.", ignore_extra=False)
    async def trbmb(self, ctx):
        """It doesn't make any sense"""
        await command_trbmb.run_trbmb(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Sends a random geek joke.", ignore_extra=False)
    async def geekjoke(self, ctx):
        """Sends a random geek joke"""
        await command_geekjoke.run_geekjoke(ctx)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Chuck Norris jokes, you dig.", ignore_extra=False)
    async def chucknorris(self, ctx):
        """Chuck Norris jokes, you dig?"""
        await command_chucknorris.run_chucknorris(ctx)

    @commands.command(brief="Press F to pay respects.", ignore_extra=False)
    @commands.cooldown(1, 2.5, type=commands.BucketType.user)
    async def f(self, ctx, *, reason=None):
        """Press F to pay respects.
        ```
        Parameters
        ------------
        reason: <Required>
            The reason to pay respects. 
            Can be a maximum of 50 characters
        ```
        """
        await command_f.run_f(ctx, reason)

    @commands.cooldown(1, 5, type=commands.BucketType.user)
    @commands.command(brief="Provides a random number fact.", ignore_extra=False)
    async def numberfact(self, ctx):
        """Provides a random number fact."""
        await command_numberfact.run_numberfact(ctx)




def setup(bot):
    bot.add_cog(VerinsLunchbox(bot))
