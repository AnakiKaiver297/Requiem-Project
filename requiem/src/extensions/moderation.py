# Copyright (C) 2019  Requiem Project
#
# Requiem is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Requiem is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Requiem.  If not, see <http://www.gnu.org/licenses/>.


import discord
from discord.ext import commands
from core.engine.utils import objects
from src.functions import command_purge


class Moderation(objects.Cog):
	"""Moderation Commands"""
	def __init__(self, bot):
		self.bot = bot
		self.name = "Moderation"


	@commands.command(brief="Purges a users messages.", ignore_extra=True)
	@commands.has_permissions(manage_messages=True)
	@commands.bot_has_permissions(manage_messages=True)
	async def purge(self, ctx, user: discord.Member, limit=100):
		"""Purges a specified number of a specified users messages.
		```
        Parameters
        ------------
        user: <Required>
            The user whose messages are to be purged. 
            Must be a user mention.
        limit: [Optional]
            The total number of messages to be purged. 
            Defaults to 100. Maximum is 100.
		```
        """
		await command_purge.run_purge(ctx, user, limit)


def setup(bot):
	bot.add_cog(Moderation(bot))