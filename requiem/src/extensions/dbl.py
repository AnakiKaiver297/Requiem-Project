import dbl
import asyncio
from discord.ext import commands


class DiscordBotsOrgAPI(commands.Cog):
    """! Handles interactions with the discordbots.org API"""

    def __init__(self, bot):
        self.bot = bot
        self.token = self.bot.config.dbl_token
        self.dblpy = dbl.DBLClient(self.bot, self.token)
        self.tasks = [
            self.update_stats
        ]

    async def update_stats(self):
        """This function runs every 30 minutes to automatically update your server count"""
        while True:
            try:
                self.bot.logger.debug('Attempting to Post Guild Count!')
                if self.bot.config.dbl_token != "":
                    sc = await self.dblpy.get_guild_count()
                    if int(sc["server_count"]) != len(self.bot.guilds):
                        try:
                            await self.dblpy.post_guild_count()
                            self.bot.logger.debug("Server Count Updated!")
                        except Exception as e:
                            self.bot.logger.exception('Failed to Post Guild Count with the Error:\n\n{}: {}'.format(type(e).__name__, e))
                    else:
                        self.bot.logger.debug("Server Side Already Up-To-Date!")
                else:
                    self.bot.logger.debug("Failed while attempting to post Guild Count! No DBL Key! You can ignore this...")
                await asyncio.sleep(1800)
            except:
                self.bot.logger.error("There was a problem posting guild count to DBL! Requiem will try again later...")
                await asyncio.sleep(3000)


def setup(bot):
    bot.add_cog(DiscordBotsOrgAPI(bot))
